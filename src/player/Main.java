package player;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import grammar.ABCMusicLexer;
import grammar.ABCMusicParser;
import sound.*;

/**
 * Main entry point of the ABC player.
 */
public class Main {

    /**
     * Plays the input file using Java MIDI API and displays
     * header information to the standard output stream.
     * 
     * @param file The name of input ABC file
     */
    public static void play(String file) {
        
        try {
            String fileContents = readFile(file);
            
            // Generate the stream of tokens from the lexer
            CharStream stream = new ANTLRInputStream(fileContents);
            ABCMusicLexer lexer = new ABCMusicLexer(stream);
            lexer.reportErrorsAsExceptions();
            TokenStream tokens = new CommonTokenStream(lexer);
            
            // Send the tokens to the parser
            ABCMusicParser parser = new ABCMusicParser(tokens);
            parser.reportErrorsAsExceptions();
            
            // Create the parse tree using the starter rule, abctune
            ParseTree tree = parser.abctune();
            
            // Walk the parse tree to generate the Song object
            ParseTreeWalker walker = new ParseTreeWalker();
            SoundListener listener = new SoundListener();
            walker.walk(listener, tree);
            
            // Retrieve the Song object from the listener, and play it!
            Song song = listener.getSong();
            song.play();
            
        } catch(IOException e) {
            
            // no such file!
            System.err.println("Failure. That file doesn't seem to exist!");
		}
        
    }

    public static void main(String[] args) {
        
        // Create the GUI and display the file chooser
        GUI gui = new GUI();
        String fileName = gui.chooseFile();
        
        fileName = "C:/Users/aanojima/Documents/6.005/abcplayer/sample_abc/megaman.abc";
        
        // Play the chosen file
        play(fileName);
    }
    
    /**
     * Read a file to a String.
     * @param filename The file to open.
     * @return A String containing the contents of the file.
     * @throws IOException If the file cannot be opened.
     */
    public static String readFile(String filename) throws IOException {
        // read in the file directly as a byte array
        byte[] byteArray = Files.readAllBytes(Paths.get(filename));
        // decode that byte array using the default charset so it's just text
        return Charset.defaultCharset().decode(ByteBuffer.wrap(byteArray)).toString();
    }
    
}
