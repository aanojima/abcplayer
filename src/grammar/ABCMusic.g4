/**
 * This file is the grammar file used by ANTLR.
 *
 * In order to compile this file, navigate to this directory
 * (<src/grammar>) and run the following command:
 *
 * java -jar ../../antlr.jar ABCMusic.g4
 */

grammar ABCMusic;

/*
 * This puts "package grammar;" at the top of the output Java files.
 * Do not change these lines unless you know what you're doing.
 */
@header {
package grammar;
}

/*
 * This adds code to the generated lexer and parser. This makes the lexer and
 * parser throw errors if they encounter invalid input. Do not change these
 * lines unless you know what you're doing.
 */
@members {
    // This method makes the lexer or parser stop running if it encounters
    // invalid input and throw a RuntimeException.
    public void reportErrorsAsExceptions() {
        removeErrorListeners();
        addErrorListener(new ExceptionThrowingErrorListener());
    }

    private static class ExceptionThrowingErrorListener extends BaseErrorListener {
        @Override
        public void syntaxError(Recognizer<?, ?> recognizer,
                Object offendingSymbol, int line, int charPositionInLine,
                String msg, RecognitionException e) {
            throw new RuntimeException(msg);
        }
    }
}

/*
 * These are the lexical rules. They define the tokens used by the lexer.
 */
KEYSHARP : '#';
DIGIT : [0-9];
OCTAVE : '\''+ | ','+;
ALPHABET: [a-zA-Z];
SPACE: ' ';
DOT: '.';
EXCLAMATION: '!';
QUESTION: '?';
UNDERSCORE: '_';
CARET: '^';
EQUALS: '=';
SLASH: '/';
ASTERISK: '*';
TILDE: '~';
DASH: '-';
SLASHDASH: '\-';
NEWLINE: '\r'? '\n';
COMMON: 'C|';
PIPE: '|';
REPEAT_START: '|:';
REPEAT_END: ':|';
BRACKETPIPE: '[|';
PIPEBRACKET: '|]';
OPENPAREN: '(';
CLOSEPAREN: ')';

/*
 * These are the parser rules. They define the structures used by the parser.
 *
 * You should make sure you have one rule that describes the entire input.
 * This is the "start rule". The start rule should end with the special
 * predefined token EOF so that it describes the entire input. Below, we've made
 * "line" the start rule.
 *
 * For more information, see
 * http://www.antlr.org/wiki/display/ANTLR4/Parser+Rules#ParserRules-StartRulesandEOF
 */

abctune : abcheader abcmusic EOF;


//header information
abcheader : fieldnumber comment* fieldtitle otherfields* fieldkey;

fieldnumber : 'X:' SPACE* DIGIT+ lineender;
fieldtitle : 'T:' SPACE* text lineender;
otherfields : fieldcomposer | fielddefaultlength | fieldmeter | fieldtempo | fieldvoice | comment;
fieldcomposer : 'C:' SPACE* text lineender;
fielddefaultlength : 'L:' SPACE* notelengthstrict lineender;
fieldmeter : 'M:' SPACE* meter lineender;
fieldtempo : 'Q:' SPACE* tempo lineender;
fieldvoice : 'V:' SPACE* text lineender;
fieldkey : 'K:' SPACE* key lineender;

key : keynote modeminor?;
modeminor : ALPHABET;
keynote : ALPHABET keyaccidental?;
keyaccidental : KEYSHARP | ALPHABET;

meter : ALPHABET | COMMON | meterfraction;
meterfraction : DIGIT+ SLASH DIGIT+;

tempo: meterfraction EQUALS DIGIT+;

//music information
abcmusic : abcline+;
abcline : element+ NEWLINE (lyricline NEWLINE)? | midtunefield | comment;
element : noteelement | tupletelement | barline | nthrepeat | SPACE;

noteelement : note | chord;

note : pitch notelength?;
pitch : accidental? ALPHABET OCTAVE?;
accidental : CARET | CARET CARET | UNDERSCORE | UNDERSCORE UNDERSCORE | EQUALS;
notelength : DIGIT* (SLASH DIGIT*)?;
notelengthstrict : DIGIT+ SLASH DIGIT+;

//tuplets
tupletelement : tupletspec noteelement+;
tupletspec : OPENPAREN DIGIT;

barline : PIPE | PIPE PIPE | BRACKETPIPE | PIPEBRACKET | REPEAT_END | REPEAT_START;
nthrepeat : '[1' | '[2';

//chords
chord : '[' note+ ']';

midtunefield : fieldvoice;

comment : '%' text? NEWLINE;
lineender : comment | NEWLINE;

lyricline : 'w:' lyrictext*;
text : (SPACE|ALPHABET|DOT|DIGIT|EXCLAMATION|QUESTION|OCTAVE|OPENPAREN|CLOSEPAREN)+ ;
lyric : (ALPHABET|DOT|EXCLAMATION|QUESTION|OPENPAREN|CLOSEPAREN|OCTAVE|TILDE|SLASHDASH)+ ;
lyrictext : SPACE+ | DASH | UNDERSCORE | ASTERISK | PIPE | lyric;