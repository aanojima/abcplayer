// Generated from ABCMusic.g4 by ANTLR 4.0

package grammar;


import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.tree.ErrorNode;

public class ABCMusicBaseListener implements ABCMusicListener {
	@Override public void enterLineender(ABCMusicParser.LineenderContext ctx) { }
	@Override public void exitLineender(ABCMusicParser.LineenderContext ctx) { }

	@Override public void enterOtherfields(ABCMusicParser.OtherfieldsContext ctx) { }
	@Override public void exitOtherfields(ABCMusicParser.OtherfieldsContext ctx) { }

	@Override public void enterLyrictext(ABCMusicParser.LyrictextContext ctx) { }
	@Override public void exitLyrictext(ABCMusicParser.LyrictextContext ctx) { }

	@Override public void enterAbctune(ABCMusicParser.AbctuneContext ctx) { }
	@Override public void exitAbctune(ABCMusicParser.AbctuneContext ctx) { }

	@Override public void enterTempo(ABCMusicParser.TempoContext ctx) { }
	@Override public void exitTempo(ABCMusicParser.TempoContext ctx) { }

	@Override public void enterMidtunefield(ABCMusicParser.MidtunefieldContext ctx) { }
	@Override public void exitMidtunefield(ABCMusicParser.MidtunefieldContext ctx) { }

	@Override public void enterChord(ABCMusicParser.ChordContext ctx) { }
	@Override public void exitChord(ABCMusicParser.ChordContext ctx) { }

	@Override public void enterFieldcomposer(ABCMusicParser.FieldcomposerContext ctx) { }
	@Override public void exitFieldcomposer(ABCMusicParser.FieldcomposerContext ctx) { }

	@Override public void enterAbcline(ABCMusicParser.AbclineContext ctx) { }
	@Override public void exitAbcline(ABCMusicParser.AbclineContext ctx) { }

	@Override public void enterAbcmusic(ABCMusicParser.AbcmusicContext ctx) { }
	@Override public void exitAbcmusic(ABCMusicParser.AbcmusicContext ctx) { }

	@Override public void enterFieldvoice(ABCMusicParser.FieldvoiceContext ctx) { }
	@Override public void exitFieldvoice(ABCMusicParser.FieldvoiceContext ctx) { }

	@Override public void enterMeter(ABCMusicParser.MeterContext ctx) { }
	@Override public void exitMeter(ABCMusicParser.MeterContext ctx) { }

	@Override public void enterFieldmeter(ABCMusicParser.FieldmeterContext ctx) { }
	@Override public void exitFieldmeter(ABCMusicParser.FieldmeterContext ctx) { }

	@Override public void enterNotelengthstrict(ABCMusicParser.NotelengthstrictContext ctx) { }
	@Override public void exitNotelengthstrict(ABCMusicParser.NotelengthstrictContext ctx) { }

	@Override public void enterNoteelement(ABCMusicParser.NoteelementContext ctx) { }
	@Override public void exitNoteelement(ABCMusicParser.NoteelementContext ctx) { }

	@Override public void enterAccidental(ABCMusicParser.AccidentalContext ctx) { }
	@Override public void exitAccidental(ABCMusicParser.AccidentalContext ctx) { }

	@Override public void enterFieldtempo(ABCMusicParser.FieldtempoContext ctx) { }
	@Override public void exitFieldtempo(ABCMusicParser.FieldtempoContext ctx) { }

	@Override public void enterMeterfraction(ABCMusicParser.MeterfractionContext ctx) { }
	@Override public void exitMeterfraction(ABCMusicParser.MeterfractionContext ctx) { }

	@Override public void enterKey(ABCMusicParser.KeyContext ctx) { }
	@Override public void exitKey(ABCMusicParser.KeyContext ctx) { }

	@Override public void enterNote(ABCMusicParser.NoteContext ctx) { }
	@Override public void exitNote(ABCMusicParser.NoteContext ctx) { }

	@Override public void enterLyricline(ABCMusicParser.LyriclineContext ctx) { }
	@Override public void exitLyricline(ABCMusicParser.LyriclineContext ctx) { }

	@Override public void enterKeynote(ABCMusicParser.KeynoteContext ctx) { }
	@Override public void exitKeynote(ABCMusicParser.KeynoteContext ctx) { }

	@Override public void enterElement(ABCMusicParser.ElementContext ctx) { }
	@Override public void exitElement(ABCMusicParser.ElementContext ctx) { }

	@Override public void enterText(ABCMusicParser.TextContext ctx) { }
	@Override public void exitText(ABCMusicParser.TextContext ctx) { }

	@Override public void enterModeminor(ABCMusicParser.ModeminorContext ctx) { }
	@Override public void exitModeminor(ABCMusicParser.ModeminorContext ctx) { }

	@Override public void enterFieldtitle(ABCMusicParser.FieldtitleContext ctx) { }
	@Override public void exitFieldtitle(ABCMusicParser.FieldtitleContext ctx) { }

	@Override public void enterFieldnumber(ABCMusicParser.FieldnumberContext ctx) { }
	@Override public void exitFieldnumber(ABCMusicParser.FieldnumberContext ctx) { }

	@Override public void enterBarline(ABCMusicParser.BarlineContext ctx) { }
	@Override public void exitBarline(ABCMusicParser.BarlineContext ctx) { }

	@Override public void enterLyric(ABCMusicParser.LyricContext ctx) { }
	@Override public void exitLyric(ABCMusicParser.LyricContext ctx) { }

	@Override public void enterNotelength(ABCMusicParser.NotelengthContext ctx) { }
	@Override public void exitNotelength(ABCMusicParser.NotelengthContext ctx) { }

	@Override public void enterNthrepeat(ABCMusicParser.NthrepeatContext ctx) { }
	@Override public void exitNthrepeat(ABCMusicParser.NthrepeatContext ctx) { }

	@Override public void enterFieldkey(ABCMusicParser.FieldkeyContext ctx) { }
	@Override public void exitFieldkey(ABCMusicParser.FieldkeyContext ctx) { }

	@Override public void enterAbcheader(ABCMusicParser.AbcheaderContext ctx) { }
	@Override public void exitAbcheader(ABCMusicParser.AbcheaderContext ctx) { }

	@Override public void enterTupletspec(ABCMusicParser.TupletspecContext ctx) { }
	@Override public void exitTupletspec(ABCMusicParser.TupletspecContext ctx) { }

	@Override public void enterTupletelement(ABCMusicParser.TupletelementContext ctx) { }
	@Override public void exitTupletelement(ABCMusicParser.TupletelementContext ctx) { }

	@Override public void enterFielddefaultlength(ABCMusicParser.FielddefaultlengthContext ctx) { }
	@Override public void exitFielddefaultlength(ABCMusicParser.FielddefaultlengthContext ctx) { }

	@Override public void enterKeyaccidental(ABCMusicParser.KeyaccidentalContext ctx) { }
	@Override public void exitKeyaccidental(ABCMusicParser.KeyaccidentalContext ctx) { }

	@Override public void enterComment(ABCMusicParser.CommentContext ctx) { }
	@Override public void exitComment(ABCMusicParser.CommentContext ctx) { }

	@Override public void enterPitch(ABCMusicParser.PitchContext ctx) { }
	@Override public void exitPitch(ABCMusicParser.PitchContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	@Override public void visitTerminal(TerminalNode node) { }
	@Override public void visitErrorNode(ErrorNode node) { }
}