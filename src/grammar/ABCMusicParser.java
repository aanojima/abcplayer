// Generated from ABCMusic.g4 by ANTLR 4.0

package grammar;

import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ABCMusicParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__13=1, T__12=2, T__11=3, T__10=4, T__9=5, T__8=6, T__7=7, T__6=8, T__5=9, 
		T__4=10, T__3=11, T__2=12, T__1=13, T__0=14, KEYSHARP=15, DIGIT=16, OCTAVE=17, 
		ALPHABET=18, SPACE=19, DOT=20, EXCLAMATION=21, QUESTION=22, UNDERSCORE=23, 
		CARET=24, EQUALS=25, SLASH=26, ASTERISK=27, TILDE=28, DASH=29, SLASHDASH=30, 
		NEWLINE=31, COMMON=32, PIPE=33, REPEAT_START=34, REPEAT_END=35, BRACKETPIPE=36, 
		PIPEBRACKET=37, OPENPAREN=38, CLOSEPAREN=39;
	public static final String[] tokenNames = {
		"<INVALID>", "']'", "'%'", "'L:'", "'C:'", "'['", "'X:'", "'V:'", "'T:'", 
		"'Q:'", "'w:'", "'K:'", "'[1'", "'M:'", "'[2'", "'#'", "DIGIT", "OCTAVE", 
		"ALPHABET", "' '", "'.'", "'!'", "'?'", "'_'", "'^'", "'='", "'/'", "'*'", 
		"'~'", "'-'", "'-'", "NEWLINE", "'C|'", "'|'", "'|:'", "':|'", "'[|'", 
		"'|]'", "'('", "')'"
	};
	public static final int
		RULE_abctune = 0, RULE_abcheader = 1, RULE_fieldnumber = 2, RULE_fieldtitle = 3, 
		RULE_otherfields = 4, RULE_fieldcomposer = 5, RULE_fielddefaultlength = 6, 
		RULE_fieldmeter = 7, RULE_fieldtempo = 8, RULE_fieldvoice = 9, RULE_fieldkey = 10, 
		RULE_key = 11, RULE_modeminor = 12, RULE_keynote = 13, RULE_keyaccidental = 14, 
		RULE_meter = 15, RULE_meterfraction = 16, RULE_tempo = 17, RULE_abcmusic = 18, 
		RULE_abcline = 19, RULE_element = 20, RULE_noteelement = 21, RULE_note = 22, 
		RULE_pitch = 23, RULE_accidental = 24, RULE_notelength = 25, RULE_notelengthstrict = 26, 
		RULE_tupletelement = 27, RULE_tupletspec = 28, RULE_barline = 29, RULE_nthrepeat = 30, 
		RULE_chord = 31, RULE_midtunefield = 32, RULE_comment = 33, RULE_lineender = 34, 
		RULE_lyricline = 35, RULE_text = 36, RULE_lyric = 37, RULE_lyrictext = 38;
	public static final String[] ruleNames = {
		"abctune", "abcheader", "fieldnumber", "fieldtitle", "otherfields", "fieldcomposer", 
		"fielddefaultlength", "fieldmeter", "fieldtempo", "fieldvoice", "fieldkey", 
		"key", "modeminor", "keynote", "keyaccidental", "meter", "meterfraction", 
		"tempo", "abcmusic", "abcline", "element", "noteelement", "note", "pitch", 
		"accidental", "notelength", "notelengthstrict", "tupletelement", "tupletspec", 
		"barline", "nthrepeat", "chord", "midtunefield", "comment", "lineender", 
		"lyricline", "text", "lyric", "lyrictext"
	};

	@Override
	public String getGrammarFileName() { return "ABCMusic.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }


	    // This method makes the lexer or parser stop running if it encounters
	    // invalid input and throw a RuntimeException.
	    public void reportErrorsAsExceptions() {
	        removeErrorListeners();
	        addErrorListener(new ExceptionThrowingErrorListener());
	    }

	    private static class ExceptionThrowingErrorListener extends BaseErrorListener {
	        @Override
	        public void syntaxError(Recognizer<?, ?> recognizer,
	                Object offendingSymbol, int line, int charPositionInLine,
	                String msg, RecognitionException e) {
	            throw new RuntimeException(msg);
	        }
	    }

	public ABCMusicParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class AbctuneContext extends ParserRuleContext {
		public AbcmusicContext abcmusic() {
			return getRuleContext(AbcmusicContext.class,0);
		}
		public AbcheaderContext abcheader() {
			return getRuleContext(AbcheaderContext.class,0);
		}
		public TerminalNode EOF() { return getToken(ABCMusicParser.EOF, 0); }
		public AbctuneContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abctune; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterAbctune(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitAbctune(this);
		}
	}

	public final AbctuneContext abctune() throws RecognitionException {
		AbctuneContext _localctx = new AbctuneContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_abctune);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78); abcheader();
			setState(79); abcmusic();
			setState(80); match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbcheaderContext extends ParserRuleContext {
		public OtherfieldsContext otherfields(int i) {
			return getRuleContext(OtherfieldsContext.class,i);
		}
		public List<OtherfieldsContext> otherfields() {
			return getRuleContexts(OtherfieldsContext.class);
		}
		public FieldkeyContext fieldkey() {
			return getRuleContext(FieldkeyContext.class,0);
		}
		public FieldtitleContext fieldtitle() {
			return getRuleContext(FieldtitleContext.class,0);
		}
		public CommentContext comment(int i) {
			return getRuleContext(CommentContext.class,i);
		}
		public List<CommentContext> comment() {
			return getRuleContexts(CommentContext.class);
		}
		public FieldnumberContext fieldnumber() {
			return getRuleContext(FieldnumberContext.class,0);
		}
		public AbcheaderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abcheader; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterAbcheader(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitAbcheader(this);
		}
	}

	public final AbcheaderContext abcheader() throws RecognitionException {
		AbcheaderContext _localctx = new AbcheaderContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_abcheader);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(82); fieldnumber();
			setState(86);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==2) {
				{
				{
				setState(83); comment();
				}
				}
				setState(88);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(89); fieldtitle();
			setState(93);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 3) | (1L << 4) | (1L << 7) | (1L << 9) | (1L << 13))) != 0)) {
				{
				{
				setState(90); otherfields();
				}
				}
				setState(95);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(96); fieldkey();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldnumberContext extends ParserRuleContext {
		public TerminalNode DIGIT(int i) {
			return getToken(ABCMusicParser.DIGIT, i);
		}
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public List<TerminalNode> DIGIT() { return getTokens(ABCMusicParser.DIGIT); }
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public FieldnumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldnumber; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFieldnumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFieldnumber(this);
		}
	}

	public final FieldnumberContext fieldnumber() throws RecognitionException {
		FieldnumberContext _localctx = new FieldnumberContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_fieldnumber);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(98); match(6);
			setState(102);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(99); match(SPACE);
				}
				}
				setState(104);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(106); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(105); match(DIGIT);
				}
				}
				setState(108); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			setState(110); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldtitleContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public TextContext text() {
			return getRuleContext(TextContext.class,0);
		}
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public FieldtitleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldtitle; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFieldtitle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFieldtitle(this);
		}
	}

	public final FieldtitleContext fieldtitle() throws RecognitionException {
		FieldtitleContext _localctx = new FieldtitleContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_fieldtitle);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(112); match(8);
			setState(116);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(113); match(SPACE);
					}
					} 
				}
				setState(118);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			setState(119); text();
			setState(120); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OtherfieldsContext extends ParserRuleContext {
		public FieldvoiceContext fieldvoice() {
			return getRuleContext(FieldvoiceContext.class,0);
		}
		public FieldmeterContext fieldmeter() {
			return getRuleContext(FieldmeterContext.class,0);
		}
		public FielddefaultlengthContext fielddefaultlength() {
			return getRuleContext(FielddefaultlengthContext.class,0);
		}
		public CommentContext comment() {
			return getRuleContext(CommentContext.class,0);
		}
		public FieldtempoContext fieldtempo() {
			return getRuleContext(FieldtempoContext.class,0);
		}
		public FieldcomposerContext fieldcomposer() {
			return getRuleContext(FieldcomposerContext.class,0);
		}
		public OtherfieldsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_otherfields; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterOtherfields(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitOtherfields(this);
		}
	}

	public final OtherfieldsContext otherfields() throws RecognitionException {
		OtherfieldsContext _localctx = new OtherfieldsContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_otherfields);
		try {
			setState(128);
			switch (_input.LA(1)) {
			case 4:
				enterOuterAlt(_localctx, 1);
				{
				setState(122); fieldcomposer();
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 2);
				{
				setState(123); fielddefaultlength();
				}
				break;
			case 13:
				enterOuterAlt(_localctx, 3);
				{
				setState(124); fieldmeter();
				}
				break;
			case 9:
				enterOuterAlt(_localctx, 4);
				{
				setState(125); fieldtempo();
				}
				break;
			case 7:
				enterOuterAlt(_localctx, 5);
				{
				setState(126); fieldvoice();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 6);
				{
				setState(127); comment();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldcomposerContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public TextContext text() {
			return getRuleContext(TextContext.class,0);
		}
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public FieldcomposerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldcomposer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFieldcomposer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFieldcomposer(this);
		}
	}

	public final FieldcomposerContext fieldcomposer() throws RecognitionException {
		FieldcomposerContext _localctx = new FieldcomposerContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_fieldcomposer);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(130); match(4);
			setState(134);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(131); match(SPACE);
					}
					} 
				}
				setState(136);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,6,_ctx);
			}
			setState(137); text();
			setState(138); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FielddefaultlengthContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public NotelengthstrictContext notelengthstrict() {
			return getRuleContext(NotelengthstrictContext.class,0);
		}
		public FielddefaultlengthContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fielddefaultlength; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFielddefaultlength(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFielddefaultlength(this);
		}
	}

	public final FielddefaultlengthContext fielddefaultlength() throws RecognitionException {
		FielddefaultlengthContext _localctx = new FielddefaultlengthContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_fielddefaultlength);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(140); match(3);
			setState(144);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(141); match(SPACE);
				}
				}
				setState(146);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(147); notelengthstrict();
			setState(148); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldmeterContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public MeterContext meter() {
			return getRuleContext(MeterContext.class,0);
		}
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public FieldmeterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldmeter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFieldmeter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFieldmeter(this);
		}
	}

	public final FieldmeterContext fieldmeter() throws RecognitionException {
		FieldmeterContext _localctx = new FieldmeterContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_fieldmeter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(150); match(13);
			setState(154);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(151); match(SPACE);
				}
				}
				setState(156);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(157); meter();
			setState(158); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldtempoContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public TempoContext tempo() {
			return getRuleContext(TempoContext.class,0);
		}
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public FieldtempoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldtempo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFieldtempo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFieldtempo(this);
		}
	}

	public final FieldtempoContext fieldtempo() throws RecognitionException {
		FieldtempoContext _localctx = new FieldtempoContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_fieldtempo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(160); match(9);
			setState(164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(161); match(SPACE);
				}
				}
				setState(166);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(167); tempo();
			setState(168); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldvoiceContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public TextContext text() {
			return getRuleContext(TextContext.class,0);
		}
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public FieldvoiceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldvoice; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFieldvoice(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFieldvoice(this);
		}
	}

	public final FieldvoiceContext fieldvoice() throws RecognitionException {
		FieldvoiceContext _localctx = new FieldvoiceContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_fieldvoice);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(170); match(7);
			setState(174);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					{
					{
					setState(171); match(SPACE);
					}
					} 
				}
				setState(176);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			setState(177); text();
			setState(178); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldkeyContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LineenderContext lineender() {
			return getRuleContext(LineenderContext.class,0);
		}
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public KeyContext key() {
			return getRuleContext(KeyContext.class,0);
		}
		public FieldkeyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldkey; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterFieldkey(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitFieldkey(this);
		}
	}

	public final FieldkeyContext fieldkey() throws RecognitionException {
		FieldkeyContext _localctx = new FieldkeyContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_fieldkey);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(180); match(11);
			setState(184);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==SPACE) {
				{
				{
				setState(181); match(SPACE);
				}
				}
				setState(186);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(187); key();
			setState(188); lineender();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyContext extends ParserRuleContext {
		public ModeminorContext modeminor() {
			return getRuleContext(ModeminorContext.class,0);
		}
		public KeynoteContext keynote() {
			return getRuleContext(KeynoteContext.class,0);
		}
		public KeyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_key; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterKey(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitKey(this);
		}
	}

	public final KeyContext key() throws RecognitionException {
		KeyContext _localctx = new KeyContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_key);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(190); keynote();
			setState(192);
			_la = _input.LA(1);
			if (_la==ALPHABET) {
				{
				setState(191); modeminor();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModeminorContext extends ParserRuleContext {
		public TerminalNode ALPHABET() { return getToken(ABCMusicParser.ALPHABET, 0); }
		public ModeminorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_modeminor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterModeminor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitModeminor(this);
		}
	}

	public final ModeminorContext modeminor() throws RecognitionException {
		ModeminorContext _localctx = new ModeminorContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_modeminor);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(194); match(ALPHABET);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeynoteContext extends ParserRuleContext {
		public KeyaccidentalContext keyaccidental() {
			return getRuleContext(KeyaccidentalContext.class,0);
		}
		public TerminalNode ALPHABET() { return getToken(ABCMusicParser.ALPHABET, 0); }
		public KeynoteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keynote; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterKeynote(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitKeynote(this);
		}
	}

	public final KeynoteContext keynote() throws RecognitionException {
		KeynoteContext _localctx = new KeynoteContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_keynote);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(196); match(ALPHABET);
			setState(198);
			switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
			case 1:
				{
				setState(197); keyaccidental();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyaccidentalContext extends ParserRuleContext {
		public TerminalNode KEYSHARP() { return getToken(ABCMusicParser.KEYSHARP, 0); }
		public TerminalNode ALPHABET() { return getToken(ABCMusicParser.ALPHABET, 0); }
		public KeyaccidentalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyaccidental; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterKeyaccidental(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitKeyaccidental(this);
		}
	}

	public final KeyaccidentalContext keyaccidental() throws RecognitionException {
		KeyaccidentalContext _localctx = new KeyaccidentalContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_keyaccidental);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			_la = _input.LA(1);
			if ( !(_la==KEYSHARP || _la==ALPHABET) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MeterContext extends ParserRuleContext {
		public TerminalNode COMMON() { return getToken(ABCMusicParser.COMMON, 0); }
		public TerminalNode ALPHABET() { return getToken(ABCMusicParser.ALPHABET, 0); }
		public MeterfractionContext meterfraction() {
			return getRuleContext(MeterfractionContext.class,0);
		}
		public MeterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_meter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterMeter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitMeter(this);
		}
	}

	public final MeterContext meter() throws RecognitionException {
		MeterContext _localctx = new MeterContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_meter);
		try {
			setState(205);
			switch (_input.LA(1)) {
			case ALPHABET:
				enterOuterAlt(_localctx, 1);
				{
				setState(202); match(ALPHABET);
				}
				break;
			case COMMON:
				enterOuterAlt(_localctx, 2);
				{
				setState(203); match(COMMON);
				}
				break;
			case DIGIT:
				enterOuterAlt(_localctx, 3);
				{
				setState(204); meterfraction();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MeterfractionContext extends ParserRuleContext {
		public TerminalNode DIGIT(int i) {
			return getToken(ABCMusicParser.DIGIT, i);
		}
		public TerminalNode SLASH() { return getToken(ABCMusicParser.SLASH, 0); }
		public List<TerminalNode> DIGIT() { return getTokens(ABCMusicParser.DIGIT); }
		public MeterfractionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_meterfraction; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterMeterfraction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitMeterfraction(this);
		}
	}

	public final MeterfractionContext meterfraction() throws RecognitionException {
		MeterfractionContext _localctx = new MeterfractionContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_meterfraction);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(208); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(207); match(DIGIT);
				}
				}
				setState(210); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			setState(212); match(SLASH);
			setState(214); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(213); match(DIGIT);
				}
				}
				setState(216); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TempoContext extends ParserRuleContext {
		public TerminalNode DIGIT(int i) {
			return getToken(ABCMusicParser.DIGIT, i);
		}
		public TerminalNode EQUALS() { return getToken(ABCMusicParser.EQUALS, 0); }
		public List<TerminalNode> DIGIT() { return getTokens(ABCMusicParser.DIGIT); }
		public MeterfractionContext meterfraction() {
			return getRuleContext(MeterfractionContext.class,0);
		}
		public TempoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tempo; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterTempo(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitTempo(this);
		}
	}

	public final TempoContext tempo() throws RecognitionException {
		TempoContext _localctx = new TempoContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_tempo);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(218); meterfraction();
			setState(219); match(EQUALS);
			setState(221); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(220); match(DIGIT);
				}
				}
				setState(223); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbcmusicContext extends ParserRuleContext {
		public AbclineContext abcline(int i) {
			return getRuleContext(AbclineContext.class,i);
		}
		public List<AbclineContext> abcline() {
			return getRuleContexts(AbclineContext.class);
		}
		public AbcmusicContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abcmusic; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterAbcmusic(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitAbcmusic(this);
		}
	}

	public final AbcmusicContext abcmusic() throws RecognitionException {
		AbcmusicContext _localctx = new AbcmusicContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_abcmusic);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(226); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(225); abcline();
				}
				}
				setState(228); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 2) | (1L << 5) | (1L << 7) | (1L << 12) | (1L << 14) | (1L << ALPHABET) | (1L << SPACE) | (1L << UNDERSCORE) | (1L << CARET) | (1L << EQUALS) | (1L << PIPE) | (1L << REPEAT_START) | (1L << REPEAT_END) | (1L << BRACKETPIPE) | (1L << PIPEBRACKET) | (1L << OPENPAREN))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AbclineContext extends ParserRuleContext {
		public List<ElementContext> element() {
			return getRuleContexts(ElementContext.class);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(ABCMusicParser.NEWLINE); }
		public ElementContext element(int i) {
			return getRuleContext(ElementContext.class,i);
		}
		public TerminalNode NEWLINE(int i) {
			return getToken(ABCMusicParser.NEWLINE, i);
		}
		public CommentContext comment() {
			return getRuleContext(CommentContext.class,0);
		}
		public MidtunefieldContext midtunefield() {
			return getRuleContext(MidtunefieldContext.class,0);
		}
		public LyriclineContext lyricline() {
			return getRuleContext(LyriclineContext.class,0);
		}
		public AbclineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_abcline; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterAbcline(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitAbcline(this);
		}
	}

	public final AbclineContext abcline() throws RecognitionException {
		AbclineContext _localctx = new AbclineContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_abcline);
		int _la;
		try {
			setState(243);
			switch (_input.LA(1)) {
			case 5:
			case 12:
			case 14:
			case ALPHABET:
			case SPACE:
			case UNDERSCORE:
			case CARET:
			case EQUALS:
			case PIPE:
			case REPEAT_START:
			case REPEAT_END:
			case BRACKETPIPE:
			case PIPEBRACKET:
			case OPENPAREN:
				enterOuterAlt(_localctx, 1);
				{
				setState(231); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(230); element();
					}
					}
					setState(233); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << 5) | (1L << 12) | (1L << 14) | (1L << ALPHABET) | (1L << SPACE) | (1L << UNDERSCORE) | (1L << CARET) | (1L << EQUALS) | (1L << PIPE) | (1L << REPEAT_START) | (1L << REPEAT_END) | (1L << BRACKETPIPE) | (1L << PIPEBRACKET) | (1L << OPENPAREN))) != 0) );
				setState(235); match(NEWLINE);
				setState(239);
				_la = _input.LA(1);
				if (_la==10) {
					{
					setState(236); lyricline();
					setState(237); match(NEWLINE);
					}
				}

				}
				break;
			case 7:
				enterOuterAlt(_localctx, 2);
				{
				setState(241); midtunefield();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 3);
				{
				setState(242); comment();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementContext extends ParserRuleContext {
		public BarlineContext barline() {
			return getRuleContext(BarlineContext.class,0);
		}
		public NthrepeatContext nthrepeat() {
			return getRuleContext(NthrepeatContext.class,0);
		}
		public TerminalNode SPACE() { return getToken(ABCMusicParser.SPACE, 0); }
		public TupletelementContext tupletelement() {
			return getRuleContext(TupletelementContext.class,0);
		}
		public NoteelementContext noteelement() {
			return getRuleContext(NoteelementContext.class,0);
		}
		public ElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitElement(this);
		}
	}

	public final ElementContext element() throws RecognitionException {
		ElementContext _localctx = new ElementContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_element);
		try {
			setState(250);
			switch (_input.LA(1)) {
			case 5:
			case ALPHABET:
			case UNDERSCORE:
			case CARET:
			case EQUALS:
				enterOuterAlt(_localctx, 1);
				{
				setState(245); noteelement();
				}
				break;
			case OPENPAREN:
				enterOuterAlt(_localctx, 2);
				{
				setState(246); tupletelement();
				}
				break;
			case PIPE:
			case REPEAT_START:
			case REPEAT_END:
			case BRACKETPIPE:
			case PIPEBRACKET:
				enterOuterAlt(_localctx, 3);
				{
				setState(247); barline();
				}
				break;
			case 12:
			case 14:
				enterOuterAlt(_localctx, 4);
				{
				setState(248); nthrepeat();
				}
				break;
			case SPACE:
				enterOuterAlt(_localctx, 5);
				{
				setState(249); match(SPACE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NoteelementContext extends ParserRuleContext {
		public ChordContext chord() {
			return getRuleContext(ChordContext.class,0);
		}
		public NoteContext note() {
			return getRuleContext(NoteContext.class,0);
		}
		public NoteelementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_noteelement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterNoteelement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitNoteelement(this);
		}
	}

	public final NoteelementContext noteelement() throws RecognitionException {
		NoteelementContext _localctx = new NoteelementContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_noteelement);
		try {
			setState(254);
			switch (_input.LA(1)) {
			case ALPHABET:
			case UNDERSCORE:
			case CARET:
			case EQUALS:
				enterOuterAlt(_localctx, 1);
				{
				setState(252); note();
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 2);
				{
				setState(253); chord();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NoteContext extends ParserRuleContext {
		public NotelengthContext notelength() {
			return getRuleContext(NotelengthContext.class,0);
		}
		public PitchContext pitch() {
			return getRuleContext(PitchContext.class,0);
		}
		public NoteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_note; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterNote(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitNote(this);
		}
	}

	public final NoteContext note() throws RecognitionException {
		NoteContext _localctx = new NoteContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_note);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(256); pitch();
			setState(258);
			switch ( getInterpreter().adaptivePredict(_input,24,_ctx) ) {
			case 1:
				{
				setState(257); notelength();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PitchContext extends ParserRuleContext {
		public TerminalNode OCTAVE() { return getToken(ABCMusicParser.OCTAVE, 0); }
		public AccidentalContext accidental() {
			return getRuleContext(AccidentalContext.class,0);
		}
		public TerminalNode ALPHABET() { return getToken(ABCMusicParser.ALPHABET, 0); }
		public PitchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pitch; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterPitch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitPitch(this);
		}
	}

	public final PitchContext pitch() throws RecognitionException {
		PitchContext _localctx = new PitchContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_pitch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(261);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << UNDERSCORE) | (1L << CARET) | (1L << EQUALS))) != 0)) {
				{
				setState(260); accidental();
				}
			}

			setState(263); match(ALPHABET);
			setState(265);
			_la = _input.LA(1);
			if (_la==OCTAVE) {
				{
				setState(264); match(OCTAVE);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AccidentalContext extends ParserRuleContext {
		public List<TerminalNode> UNDERSCORE() { return getTokens(ABCMusicParser.UNDERSCORE); }
		public TerminalNode EQUALS() { return getToken(ABCMusicParser.EQUALS, 0); }
		public TerminalNode UNDERSCORE(int i) {
			return getToken(ABCMusicParser.UNDERSCORE, i);
		}
		public TerminalNode CARET(int i) {
			return getToken(ABCMusicParser.CARET, i);
		}
		public List<TerminalNode> CARET() { return getTokens(ABCMusicParser.CARET); }
		public AccidentalContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_accidental; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterAccidental(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitAccidental(this);
		}
	}

	public final AccidentalContext accidental() throws RecognitionException {
		AccidentalContext _localctx = new AccidentalContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_accidental);
		try {
			setState(274);
			switch ( getInterpreter().adaptivePredict(_input,27,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(267); match(CARET);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(268); match(CARET);
				setState(269); match(CARET);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(270); match(UNDERSCORE);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(271); match(UNDERSCORE);
				setState(272); match(UNDERSCORE);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(273); match(EQUALS);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotelengthContext extends ParserRuleContext {
		public TerminalNode DIGIT(int i) {
			return getToken(ABCMusicParser.DIGIT, i);
		}
		public TerminalNode SLASH() { return getToken(ABCMusicParser.SLASH, 0); }
		public List<TerminalNode> DIGIT() { return getTokens(ABCMusicParser.DIGIT); }
		public NotelengthContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notelength; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterNotelength(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitNotelength(this);
		}
	}

	public final NotelengthContext notelength() throws RecognitionException {
		NotelengthContext _localctx = new NotelengthContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_notelength);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(279);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==DIGIT) {
				{
				{
				setState(276); match(DIGIT);
				}
				}
				setState(281);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(289);
			_la = _input.LA(1);
			if (_la==SLASH) {
				{
				setState(282); match(SLASH);
				setState(286);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==DIGIT) {
					{
					{
					setState(283); match(DIGIT);
					}
					}
					setState(288);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NotelengthstrictContext extends ParserRuleContext {
		public TerminalNode DIGIT(int i) {
			return getToken(ABCMusicParser.DIGIT, i);
		}
		public TerminalNode SLASH() { return getToken(ABCMusicParser.SLASH, 0); }
		public List<TerminalNode> DIGIT() { return getTokens(ABCMusicParser.DIGIT); }
		public NotelengthstrictContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_notelengthstrict; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterNotelengthstrict(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitNotelengthstrict(this);
		}
	}

	public final NotelengthstrictContext notelengthstrict() throws RecognitionException {
		NotelengthstrictContext _localctx = new NotelengthstrictContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_notelengthstrict);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(292); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(291); match(DIGIT);
				}
				}
				setState(294); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			setState(296); match(SLASH);
			setState(298); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(297); match(DIGIT);
				}
				}
				setState(300); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TupletelementContext extends ParserRuleContext {
		public TupletspecContext tupletspec() {
			return getRuleContext(TupletspecContext.class,0);
		}
		public NoteelementContext noteelement(int i) {
			return getRuleContext(NoteelementContext.class,i);
		}
		public List<NoteelementContext> noteelement() {
			return getRuleContexts(NoteelementContext.class);
		}
		public TupletelementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tupletelement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterTupletelement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitTupletelement(this);
		}
	}

	public final TupletelementContext tupletelement() throws RecognitionException {
		TupletelementContext _localctx = new TupletelementContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_tupletelement);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(302); tupletspec();
			setState(304); 
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(303); noteelement();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(306); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,33,_ctx);
			} while ( _alt!=2 && _alt!=-1 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TupletspecContext extends ParserRuleContext {
		public TerminalNode DIGIT() { return getToken(ABCMusicParser.DIGIT, 0); }
		public TerminalNode OPENPAREN() { return getToken(ABCMusicParser.OPENPAREN, 0); }
		public TupletspecContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_tupletspec; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterTupletspec(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitTupletspec(this);
		}
	}

	public final TupletspecContext tupletspec() throws RecognitionException {
		TupletspecContext _localctx = new TupletspecContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_tupletspec);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(308); match(OPENPAREN);
			setState(309); match(DIGIT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BarlineContext extends ParserRuleContext {
		public TerminalNode REPEAT_END() { return getToken(ABCMusicParser.REPEAT_END, 0); }
		public TerminalNode BRACKETPIPE() { return getToken(ABCMusicParser.BRACKETPIPE, 0); }
		public List<TerminalNode> PIPE() { return getTokens(ABCMusicParser.PIPE); }
		public TerminalNode REPEAT_START() { return getToken(ABCMusicParser.REPEAT_START, 0); }
		public TerminalNode PIPE(int i) {
			return getToken(ABCMusicParser.PIPE, i);
		}
		public TerminalNode PIPEBRACKET() { return getToken(ABCMusicParser.PIPEBRACKET, 0); }
		public BarlineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_barline; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterBarline(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitBarline(this);
		}
	}

	public final BarlineContext barline() throws RecognitionException {
		BarlineContext _localctx = new BarlineContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_barline);
		try {
			setState(318);
			switch ( getInterpreter().adaptivePredict(_input,34,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(311); match(PIPE);
				}
				break;

			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(312); match(PIPE);
				setState(313); match(PIPE);
				}
				break;

			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(314); match(BRACKETPIPE);
				}
				break;

			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(315); match(PIPEBRACKET);
				}
				break;

			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(316); match(REPEAT_END);
				}
				break;

			case 6:
				enterOuterAlt(_localctx, 6);
				{
				setState(317); match(REPEAT_START);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NthrepeatContext extends ParserRuleContext {
		public NthrepeatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_nthrepeat; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterNthrepeat(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitNthrepeat(this);
		}
	}

	public final NthrepeatContext nthrepeat() throws RecognitionException {
		NthrepeatContext _localctx = new NthrepeatContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_nthrepeat);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(320);
			_la = _input.LA(1);
			if ( !(_la==12 || _la==14) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChordContext extends ParserRuleContext {
		public NoteContext note(int i) {
			return getRuleContext(NoteContext.class,i);
		}
		public List<NoteContext> note() {
			return getRuleContexts(NoteContext.class);
		}
		public ChordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_chord; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterChord(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitChord(this);
		}
	}

	public final ChordContext chord() throws RecognitionException {
		ChordContext _localctx = new ChordContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_chord);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(322); match(5);
			setState(324); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(323); note();
				}
				}
				setState(326); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ALPHABET) | (1L << UNDERSCORE) | (1L << CARET) | (1L << EQUALS))) != 0) );
			setState(328); match(1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MidtunefieldContext extends ParserRuleContext {
		public FieldvoiceContext fieldvoice() {
			return getRuleContext(FieldvoiceContext.class,0);
		}
		public MidtunefieldContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_midtunefield; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterMidtunefield(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitMidtunefield(this);
		}
	}

	public final MidtunefieldContext midtunefield() throws RecognitionException {
		MidtunefieldContext _localctx = new MidtunefieldContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_midtunefield);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(330); fieldvoice();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CommentContext extends ParserRuleContext {
		public TextContext text() {
			return getRuleContext(TextContext.class,0);
		}
		public TerminalNode NEWLINE() { return getToken(ABCMusicParser.NEWLINE, 0); }
		public CommentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_comment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterComment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitComment(this);
		}
	}

	public final CommentContext comment() throws RecognitionException {
		CommentContext _localctx = new CommentContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_comment);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(332); match(2);
			setState(334);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DIGIT) | (1L << OCTAVE) | (1L << ALPHABET) | (1L << SPACE) | (1L << DOT) | (1L << EXCLAMATION) | (1L << QUESTION) | (1L << OPENPAREN) | (1L << CLOSEPAREN))) != 0)) {
				{
				setState(333); text();
				}
			}

			setState(336); match(NEWLINE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LineenderContext extends ParserRuleContext {
		public TerminalNode NEWLINE() { return getToken(ABCMusicParser.NEWLINE, 0); }
		public CommentContext comment() {
			return getRuleContext(CommentContext.class,0);
		}
		public LineenderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lineender; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterLineender(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitLineender(this);
		}
	}

	public final LineenderContext lineender() throws RecognitionException {
		LineenderContext _localctx = new LineenderContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_lineender);
		try {
			setState(340);
			switch (_input.LA(1)) {
			case 2:
				enterOuterAlt(_localctx, 1);
				{
				setState(338); comment();
				}
				break;
			case NEWLINE:
				enterOuterAlt(_localctx, 2);
				{
				setState(339); match(NEWLINE);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LyriclineContext extends ParserRuleContext {
		public LyrictextContext lyrictext(int i) {
			return getRuleContext(LyrictextContext.class,i);
		}
		public List<LyrictextContext> lyrictext() {
			return getRuleContexts(LyrictextContext.class);
		}
		public LyriclineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lyricline; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterLyricline(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitLyricline(this);
		}
	}

	public final LyriclineContext lyricline() throws RecognitionException {
		LyriclineContext _localctx = new LyriclineContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_lyricline);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(342); match(10);
			setState(346);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OCTAVE) | (1L << ALPHABET) | (1L << SPACE) | (1L << DOT) | (1L << EXCLAMATION) | (1L << QUESTION) | (1L << UNDERSCORE) | (1L << ASTERISK) | (1L << TILDE) | (1L << DASH) | (1L << SLASHDASH) | (1L << PIPE) | (1L << OPENPAREN) | (1L << CLOSEPAREN))) != 0)) {
				{
				{
				setState(343); lyrictext();
				}
				}
				setState(348);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TextContext extends ParserRuleContext {
		public TerminalNode DIGIT(int i) {
			return getToken(ABCMusicParser.DIGIT, i);
		}
		public List<TerminalNode> CLOSEPAREN() { return getTokens(ABCMusicParser.CLOSEPAREN); }
		public TerminalNode DOT(int i) {
			return getToken(ABCMusicParser.DOT, i);
		}
		public List<TerminalNode> EXCLAMATION() { return getTokens(ABCMusicParser.EXCLAMATION); }
		public List<TerminalNode> QUESTION() { return getTokens(ABCMusicParser.QUESTION); }
		public List<TerminalNode> ALPHABET() { return getTokens(ABCMusicParser.ALPHABET); }
		public TerminalNode EXCLAMATION(int i) {
			return getToken(ABCMusicParser.EXCLAMATION, i);
		}
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public List<TerminalNode> OCTAVE() { return getTokens(ABCMusicParser.OCTAVE); }
		public TerminalNode OPENPAREN(int i) {
			return getToken(ABCMusicParser.OPENPAREN, i);
		}
		public TerminalNode OCTAVE(int i) {
			return getToken(ABCMusicParser.OCTAVE, i);
		}
		public List<TerminalNode> DIGIT() { return getTokens(ABCMusicParser.DIGIT); }
		public List<TerminalNode> OPENPAREN() { return getTokens(ABCMusicParser.OPENPAREN); }
		public TerminalNode CLOSEPAREN(int i) {
			return getToken(ABCMusicParser.CLOSEPAREN, i);
		}
		public List<TerminalNode> DOT() { return getTokens(ABCMusicParser.DOT); }
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public TerminalNode QUESTION(int i) {
			return getToken(ABCMusicParser.QUESTION, i);
		}
		public TerminalNode ALPHABET(int i) {
			return getToken(ABCMusicParser.ALPHABET, i);
		}
		public TextContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_text; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterText(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitText(this);
		}
	}

	public final TextContext text() throws RecognitionException {
		TextContext _localctx = new TextContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_text);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(350); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(349);
				_la = _input.LA(1);
				if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DIGIT) | (1L << OCTAVE) | (1L << ALPHABET) | (1L << SPACE) | (1L << DOT) | (1L << EXCLAMATION) | (1L << QUESTION) | (1L << OPENPAREN) | (1L << CLOSEPAREN))) != 0)) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				}
				setState(352); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << DIGIT) | (1L << OCTAVE) | (1L << ALPHABET) | (1L << SPACE) | (1L << DOT) | (1L << EXCLAMATION) | (1L << QUESTION) | (1L << OPENPAREN) | (1L << CLOSEPAREN))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LyricContext extends ParserRuleContext {
		public List<TerminalNode> CLOSEPAREN() { return getTokens(ABCMusicParser.CLOSEPAREN); }
		public TerminalNode DOT(int i) {
			return getToken(ABCMusicParser.DOT, i);
		}
		public List<TerminalNode> EXCLAMATION() { return getTokens(ABCMusicParser.EXCLAMATION); }
		public List<TerminalNode> QUESTION() { return getTokens(ABCMusicParser.QUESTION); }
		public TerminalNode SLASHDASH(int i) {
			return getToken(ABCMusicParser.SLASHDASH, i);
		}
		public List<TerminalNode> ALPHABET() { return getTokens(ABCMusicParser.ALPHABET); }
		public List<TerminalNode> TILDE() { return getTokens(ABCMusicParser.TILDE); }
		public TerminalNode EXCLAMATION(int i) {
			return getToken(ABCMusicParser.EXCLAMATION, i);
		}
		public List<TerminalNode> OCTAVE() { return getTokens(ABCMusicParser.OCTAVE); }
		public TerminalNode OPENPAREN(int i) {
			return getToken(ABCMusicParser.OPENPAREN, i);
		}
		public TerminalNode OCTAVE(int i) {
			return getToken(ABCMusicParser.OCTAVE, i);
		}
		public List<TerminalNode> OPENPAREN() { return getTokens(ABCMusicParser.OPENPAREN); }
		public List<TerminalNode> DOT() { return getTokens(ABCMusicParser.DOT); }
		public TerminalNode CLOSEPAREN(int i) {
			return getToken(ABCMusicParser.CLOSEPAREN, i);
		}
		public TerminalNode QUESTION(int i) {
			return getToken(ABCMusicParser.QUESTION, i);
		}
		public List<TerminalNode> SLASHDASH() { return getTokens(ABCMusicParser.SLASHDASH); }
		public TerminalNode TILDE(int i) {
			return getToken(ABCMusicParser.TILDE, i);
		}
		public TerminalNode ALPHABET(int i) {
			return getToken(ABCMusicParser.ALPHABET, i);
		}
		public LyricContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lyric; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterLyric(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitLyric(this);
		}
	}

	public final LyricContext lyric() throws RecognitionException {
		LyricContext _localctx = new LyricContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_lyric);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(355); 
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
			do {
				switch (_alt) {
				case 1:
					{
					{
					setState(354);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << OCTAVE) | (1L << ALPHABET) | (1L << DOT) | (1L << EXCLAMATION) | (1L << QUESTION) | (1L << TILDE) | (1L << SLASHDASH) | (1L << OPENPAREN) | (1L << CLOSEPAREN))) != 0)) ) {
					_errHandler.recoverInline(this);
					}
					consume();
					}
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				setState(357); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,40,_ctx);
			} while ( _alt!=2 && _alt!=-1 );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LyrictextContext extends ParserRuleContext {
		public TerminalNode SPACE(int i) {
			return getToken(ABCMusicParser.SPACE, i);
		}
		public LyricContext lyric() {
			return getRuleContext(LyricContext.class,0);
		}
		public TerminalNode PIPE() { return getToken(ABCMusicParser.PIPE, 0); }
		public TerminalNode UNDERSCORE() { return getToken(ABCMusicParser.UNDERSCORE, 0); }
		public TerminalNode DASH() { return getToken(ABCMusicParser.DASH, 0); }
		public List<TerminalNode> SPACE() { return getTokens(ABCMusicParser.SPACE); }
		public TerminalNode ASTERISK() { return getToken(ABCMusicParser.ASTERISK, 0); }
		public LyrictextContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_lyrictext; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).enterLyrictext(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ABCMusicListener ) ((ABCMusicListener)listener).exitLyrictext(this);
		}
	}

	public final LyrictextContext lyrictext() throws RecognitionException {
		LyrictextContext _localctx = new LyrictextContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_lyrictext);
		try {
			int _alt;
			setState(369);
			switch (_input.LA(1)) {
			case SPACE:
				enterOuterAlt(_localctx, 1);
				{
				setState(360); 
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
				do {
					switch (_alt) {
					case 1:
						{
						{
						setState(359); match(SPACE);
						}
						}
						break;
					default:
						throw new NoViableAltException(this);
					}
					setState(362); 
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input,41,_ctx);
				} while ( _alt!=2 && _alt!=-1 );
				}
				break;
			case DASH:
				enterOuterAlt(_localctx, 2);
				{
				setState(364); match(DASH);
				}
				break;
			case UNDERSCORE:
				enterOuterAlt(_localctx, 3);
				{
				setState(365); match(UNDERSCORE);
				}
				break;
			case ASTERISK:
				enterOuterAlt(_localctx, 4);
				{
				setState(366); match(ASTERISK);
				}
				break;
			case PIPE:
				enterOuterAlt(_localctx, 5);
				{
				setState(367); match(PIPE);
				}
				break;
			case OCTAVE:
			case ALPHABET:
			case DOT:
			case EXCLAMATION:
			case QUESTION:
			case TILDE:
			case SLASHDASH:
			case OPENPAREN:
			case CLOSEPAREN:
				enterOuterAlt(_localctx, 6);
				{
				setState(368); lyric();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\2\3)\u0176\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4"+
		"\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20"+
		"\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27"+
		"\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36"+
		"\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\3"+
		"\2\3\2\3\2\3\2\3\3\3\3\7\3W\n\3\f\3\16\3Z\13\3\3\3\3\3\7\3^\n\3\f\3\16"+
		"\3a\13\3\3\3\3\3\3\4\3\4\7\4g\n\4\f\4\16\4j\13\4\3\4\6\4m\n\4\r\4\16\4"+
		"n\3\4\3\4\3\5\3\5\7\5u\n\5\f\5\16\5x\13\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\5\6\u0083\n\6\3\7\3\7\7\7\u0087\n\7\f\7\16\7\u008a\13\7\3\7\3"+
		"\7\3\7\3\b\3\b\7\b\u0091\n\b\f\b\16\b\u0094\13\b\3\b\3\b\3\b\3\t\3\t\7"+
		"\t\u009b\n\t\f\t\16\t\u009e\13\t\3\t\3\t\3\t\3\n\3\n\7\n\u00a5\n\n\f\n"+
		"\16\n\u00a8\13\n\3\n\3\n\3\n\3\13\3\13\7\13\u00af\n\13\f\13\16\13\u00b2"+
		"\13\13\3\13\3\13\3\13\3\f\3\f\7\f\u00b9\n\f\f\f\16\f\u00bc\13\f\3\f\3"+
		"\f\3\f\3\r\3\r\5\r\u00c3\n\r\3\16\3\16\3\17\3\17\5\17\u00c9\n\17\3\20"+
		"\3\20\3\21\3\21\3\21\5\21\u00d0\n\21\3\22\6\22\u00d3\n\22\r\22\16\22\u00d4"+
		"\3\22\3\22\6\22\u00d9\n\22\r\22\16\22\u00da\3\23\3\23\3\23\6\23\u00e0"+
		"\n\23\r\23\16\23\u00e1\3\24\6\24\u00e5\n\24\r\24\16\24\u00e6\3\25\6\25"+
		"\u00ea\n\25\r\25\16\25\u00eb\3\25\3\25\3\25\3\25\5\25\u00f2\n\25\3\25"+
		"\3\25\5\25\u00f6\n\25\3\26\3\26\3\26\3\26\3\26\5\26\u00fd\n\26\3\27\3"+
		"\27\5\27\u0101\n\27\3\30\3\30\5\30\u0105\n\30\3\31\5\31\u0108\n\31\3\31"+
		"\3\31\5\31\u010c\n\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32\5\32\u0115\n"+
		"\32\3\33\7\33\u0118\n\33\f\33\16\33\u011b\13\33\3\33\3\33\7\33\u011f\n"+
		"\33\f\33\16\33\u0122\13\33\5\33\u0124\n\33\3\34\6\34\u0127\n\34\r\34\16"+
		"\34\u0128\3\34\3\34\6\34\u012d\n\34\r\34\16\34\u012e\3\35\3\35\6\35\u0133"+
		"\n\35\r\35\16\35\u0134\3\36\3\36\3\36\3\37\3\37\3\37\3\37\3\37\3\37\3"+
		"\37\5\37\u0141\n\37\3 \3 \3!\3!\6!\u0147\n!\r!\16!\u0148\3!\3!\3\"\3\""+
		"\3#\3#\5#\u0151\n#\3#\3#\3$\3$\5$\u0157\n$\3%\3%\7%\u015b\n%\f%\16%\u015e"+
		"\13%\3&\6&\u0161\n&\r&\16&\u0162\3\'\6\'\u0166\n\'\r\'\16\'\u0167\3(\6"+
		"(\u016b\n(\r(\16(\u016c\3(\3(\3(\3(\3(\5(\u0174\n(\3(\2)\2\4\6\b\n\f\16"+
		"\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLN\2\6\4\21\21"+
		"\24\24\4\16\16\20\20\4\22\30()\7\23\24\26\30\36\36  ()\u018d\2P\3\2\2"+
		"\2\4T\3\2\2\2\6d\3\2\2\2\br\3\2\2\2\n\u0082\3\2\2\2\f\u0084\3\2\2\2\16"+
		"\u008e\3\2\2\2\20\u0098\3\2\2\2\22\u00a2\3\2\2\2\24\u00ac\3\2\2\2\26\u00b6"+
		"\3\2\2\2\30\u00c0\3\2\2\2\32\u00c4\3\2\2\2\34\u00c6\3\2\2\2\36\u00ca\3"+
		"\2\2\2 \u00cf\3\2\2\2\"\u00d2\3\2\2\2$\u00dc\3\2\2\2&\u00e4\3\2\2\2(\u00f5"+
		"\3\2\2\2*\u00fc\3\2\2\2,\u0100\3\2\2\2.\u0102\3\2\2\2\60\u0107\3\2\2\2"+
		"\62\u0114\3\2\2\2\64\u0119\3\2\2\2\66\u0126\3\2\2\28\u0130\3\2\2\2:\u0136"+
		"\3\2\2\2<\u0140\3\2\2\2>\u0142\3\2\2\2@\u0144\3\2\2\2B\u014c\3\2\2\2D"+
		"\u014e\3\2\2\2F\u0156\3\2\2\2H\u0158\3\2\2\2J\u0160\3\2\2\2L\u0165\3\2"+
		"\2\2N\u0173\3\2\2\2PQ\5\4\3\2QR\5&\24\2RS\7\1\2\2S\3\3\2\2\2TX\5\6\4\2"+
		"UW\5D#\2VU\3\2\2\2WZ\3\2\2\2XV\3\2\2\2XY\3\2\2\2Y[\3\2\2\2ZX\3\2\2\2["+
		"_\5\b\5\2\\^\5\n\6\2]\\\3\2\2\2^a\3\2\2\2_]\3\2\2\2_`\3\2\2\2`b\3\2\2"+
		"\2a_\3\2\2\2bc\5\26\f\2c\5\3\2\2\2dh\7\b\2\2eg\7\25\2\2fe\3\2\2\2gj\3"+
		"\2\2\2hf\3\2\2\2hi\3\2\2\2il\3\2\2\2jh\3\2\2\2km\7\22\2\2lk\3\2\2\2mn"+
		"\3\2\2\2nl\3\2\2\2no\3\2\2\2op\3\2\2\2pq\5F$\2q\7\3\2\2\2rv\7\n\2\2su"+
		"\7\25\2\2ts\3\2\2\2ux\3\2\2\2vt\3\2\2\2vw\3\2\2\2wy\3\2\2\2xv\3\2\2\2"+
		"yz\5J&\2z{\5F$\2{\t\3\2\2\2|\u0083\5\f\7\2}\u0083\5\16\b\2~\u0083\5\20"+
		"\t\2\177\u0083\5\22\n\2\u0080\u0083\5\24\13\2\u0081\u0083\5D#\2\u0082"+
		"|\3\2\2\2\u0082}\3\2\2\2\u0082~\3\2\2\2\u0082\177\3\2\2\2\u0082\u0080"+
		"\3\2\2\2\u0082\u0081\3\2\2\2\u0083\13\3\2\2\2\u0084\u0088\7\6\2\2\u0085"+
		"\u0087\7\25\2\2\u0086\u0085\3\2\2\2\u0087\u008a\3\2\2\2\u0088\u0086\3"+
		"\2\2\2\u0088\u0089\3\2\2\2\u0089\u008b\3\2\2\2\u008a\u0088\3\2\2\2\u008b"+
		"\u008c\5J&\2\u008c\u008d\5F$\2\u008d\r\3\2\2\2\u008e\u0092\7\5\2\2\u008f"+
		"\u0091\7\25\2\2\u0090\u008f\3\2\2\2\u0091\u0094\3\2\2\2\u0092\u0090\3"+
		"\2\2\2\u0092\u0093\3\2\2\2\u0093\u0095\3\2\2\2\u0094\u0092\3\2\2\2\u0095"+
		"\u0096\5\66\34\2\u0096\u0097\5F$\2\u0097\17\3\2\2\2\u0098\u009c\7\17\2"+
		"\2\u0099\u009b\7\25\2\2\u009a\u0099\3\2\2\2\u009b\u009e\3\2\2\2\u009c"+
		"\u009a\3\2\2\2\u009c\u009d\3\2\2\2\u009d\u009f\3\2\2\2\u009e\u009c\3\2"+
		"\2\2\u009f\u00a0\5 \21\2\u00a0\u00a1\5F$\2\u00a1\21\3\2\2\2\u00a2\u00a6"+
		"\7\13\2\2\u00a3\u00a5\7\25\2\2\u00a4\u00a3\3\2\2\2\u00a5\u00a8\3\2\2\2"+
		"\u00a6\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7\u00a9\3\2\2\2\u00a8\u00a6"+
		"\3\2\2\2\u00a9\u00aa\5$\23\2\u00aa\u00ab\5F$\2\u00ab\23\3\2\2\2\u00ac"+
		"\u00b0\7\t\2\2\u00ad\u00af\7\25\2\2\u00ae\u00ad\3\2\2\2\u00af\u00b2\3"+
		"\2\2\2\u00b0\u00ae\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1\u00b3\3\2\2\2\u00b2"+
		"\u00b0\3\2\2\2\u00b3\u00b4\5J&\2\u00b4\u00b5\5F$\2\u00b5\25\3\2\2\2\u00b6"+
		"\u00ba\7\r\2\2\u00b7\u00b9\7\25\2\2\u00b8\u00b7\3\2\2\2\u00b9\u00bc\3"+
		"\2\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00bb\3\2\2\2\u00bb\u00bd\3\2\2\2\u00bc"+
		"\u00ba\3\2\2\2\u00bd\u00be\5\30\r\2\u00be\u00bf\5F$\2\u00bf\27\3\2\2\2"+
		"\u00c0\u00c2\5\34\17\2\u00c1\u00c3\5\32\16\2\u00c2\u00c1\3\2\2\2\u00c2"+
		"\u00c3\3\2\2\2\u00c3\31\3\2\2\2\u00c4\u00c5\7\24\2\2\u00c5\33\3\2\2\2"+
		"\u00c6\u00c8\7\24\2\2\u00c7\u00c9\5\36\20\2\u00c8\u00c7\3\2\2\2\u00c8"+
		"\u00c9\3\2\2\2\u00c9\35\3\2\2\2\u00ca\u00cb\t\2\2\2\u00cb\37\3\2\2\2\u00cc"+
		"\u00d0\7\24\2\2\u00cd\u00d0\7\"\2\2\u00ce\u00d0\5\"\22\2\u00cf\u00cc\3"+
		"\2\2\2\u00cf\u00cd\3\2\2\2\u00cf\u00ce\3\2\2\2\u00d0!\3\2\2\2\u00d1\u00d3"+
		"\7\22\2\2\u00d2\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d2\3\2\2\2"+
		"\u00d4\u00d5\3\2\2\2\u00d5\u00d6\3\2\2\2\u00d6\u00d8\7\34\2\2\u00d7\u00d9"+
		"\7\22\2\2\u00d8\u00d7\3\2\2\2\u00d9\u00da\3\2\2\2\u00da\u00d8\3\2\2\2"+
		"\u00da\u00db\3\2\2\2\u00db#\3\2\2\2\u00dc\u00dd\5\"\22\2\u00dd\u00df\7"+
		"\33\2\2\u00de\u00e0\7\22\2\2\u00df\u00de\3\2\2\2\u00e0\u00e1\3\2\2\2\u00e1"+
		"\u00df\3\2\2\2\u00e1\u00e2\3\2\2\2\u00e2%\3\2\2\2\u00e3\u00e5\5(\25\2"+
		"\u00e4\u00e3\3\2\2\2\u00e5\u00e6\3\2\2\2\u00e6\u00e4\3\2\2\2\u00e6\u00e7"+
		"\3\2\2\2\u00e7\'\3\2\2\2\u00e8\u00ea\5*\26\2\u00e9\u00e8\3\2\2\2\u00ea"+
		"\u00eb\3\2\2\2\u00eb\u00e9\3\2\2\2\u00eb\u00ec\3\2\2\2\u00ec\u00ed\3\2"+
		"\2\2\u00ed\u00f1\7!\2\2\u00ee\u00ef\5H%\2\u00ef\u00f0\7!\2\2\u00f0\u00f2"+
		"\3\2\2\2\u00f1\u00ee\3\2\2\2\u00f1\u00f2\3\2\2\2\u00f2\u00f6\3\2\2\2\u00f3"+
		"\u00f6\5B\"\2\u00f4\u00f6\5D#\2\u00f5\u00e9\3\2\2\2\u00f5\u00f3\3\2\2"+
		"\2\u00f5\u00f4\3\2\2\2\u00f6)\3\2\2\2\u00f7\u00fd\5,\27\2\u00f8\u00fd"+
		"\58\35\2\u00f9\u00fd\5<\37\2\u00fa\u00fd\5> \2\u00fb\u00fd\7\25\2\2\u00fc"+
		"\u00f7\3\2\2\2\u00fc\u00f8\3\2\2\2\u00fc\u00f9\3\2\2\2\u00fc\u00fa\3\2"+
		"\2\2\u00fc\u00fb\3\2\2\2\u00fd+\3\2\2\2\u00fe\u0101\5.\30\2\u00ff\u0101"+
		"\5@!\2\u0100\u00fe\3\2\2\2\u0100\u00ff\3\2\2\2\u0101-\3\2\2\2\u0102\u0104"+
		"\5\60\31\2\u0103\u0105\5\64\33\2\u0104\u0103\3\2\2\2\u0104\u0105\3\2\2"+
		"\2\u0105/\3\2\2\2\u0106\u0108\5\62\32\2\u0107\u0106\3\2\2\2\u0107\u0108"+
		"\3\2\2\2\u0108\u0109\3\2\2\2\u0109\u010b\7\24\2\2\u010a\u010c\7\23\2\2"+
		"\u010b\u010a\3\2\2\2\u010b\u010c\3\2\2\2\u010c\61\3\2\2\2\u010d\u0115"+
		"\7\32\2\2\u010e\u010f\7\32\2\2\u010f\u0115\7\32\2\2\u0110\u0115\7\31\2"+
		"\2\u0111\u0112\7\31\2\2\u0112\u0115\7\31\2\2\u0113\u0115\7\33\2\2\u0114"+
		"\u010d\3\2\2\2\u0114\u010e\3\2\2\2\u0114\u0110\3\2\2\2\u0114\u0111\3\2"+
		"\2\2\u0114\u0113\3\2\2\2\u0115\63\3\2\2\2\u0116\u0118\7\22\2\2\u0117\u0116"+
		"\3\2\2\2\u0118\u011b\3\2\2\2\u0119\u0117\3\2\2\2\u0119\u011a\3\2\2\2\u011a"+
		"\u0123\3\2\2\2\u011b\u0119\3\2\2\2\u011c\u0120\7\34\2\2\u011d\u011f\7"+
		"\22\2\2\u011e\u011d\3\2\2\2\u011f\u0122\3\2\2\2\u0120\u011e\3\2\2\2\u0120"+
		"\u0121\3\2\2\2\u0121\u0124\3\2\2\2\u0122\u0120\3\2\2\2\u0123\u011c\3\2"+
		"\2\2\u0123\u0124\3\2\2\2\u0124\65\3\2\2\2\u0125\u0127\7\22\2\2\u0126\u0125"+
		"\3\2\2\2\u0127\u0128\3\2\2\2\u0128\u0126\3\2\2\2\u0128\u0129\3\2\2\2\u0129"+
		"\u012a\3\2\2\2\u012a\u012c\7\34\2\2\u012b\u012d\7\22\2\2\u012c\u012b\3"+
		"\2\2\2\u012d\u012e\3\2\2\2\u012e\u012c\3\2\2\2\u012e\u012f\3\2\2\2\u012f"+
		"\67\3\2\2\2\u0130\u0132\5:\36\2\u0131\u0133\5,\27\2\u0132\u0131\3\2\2"+
		"\2\u0133\u0134\3\2\2\2\u0134\u0132\3\2\2\2\u0134\u0135\3\2\2\2\u01359"+
		"\3\2\2\2\u0136\u0137\7(\2\2\u0137\u0138\7\22\2\2\u0138;\3\2\2\2\u0139"+
		"\u0141\7#\2\2\u013a\u013b\7#\2\2\u013b\u0141\7#\2\2\u013c\u0141\7&\2\2"+
		"\u013d\u0141\7\'\2\2\u013e\u0141\7%\2\2\u013f\u0141\7$\2\2\u0140\u0139"+
		"\3\2\2\2\u0140\u013a\3\2\2\2\u0140\u013c\3\2\2\2\u0140\u013d\3\2\2\2\u0140"+
		"\u013e\3\2\2\2\u0140\u013f\3\2\2\2\u0141=\3\2\2\2\u0142\u0143\t\3\2\2"+
		"\u0143?\3\2\2\2\u0144\u0146\7\7\2\2\u0145\u0147\5.\30\2\u0146\u0145\3"+
		"\2\2\2\u0147\u0148\3\2\2\2\u0148\u0146\3\2\2\2\u0148\u0149\3\2\2\2\u0149"+
		"\u014a\3\2\2\2\u014a\u014b\7\3\2\2\u014bA\3\2\2\2\u014c\u014d\5\24\13"+
		"\2\u014dC\3\2\2\2\u014e\u0150\7\4\2\2\u014f\u0151\5J&\2\u0150\u014f\3"+
		"\2\2\2\u0150\u0151\3\2\2\2\u0151\u0152\3\2\2\2\u0152\u0153\7!\2\2\u0153"+
		"E\3\2\2\2\u0154\u0157\5D#\2\u0155\u0157\7!\2\2\u0156\u0154\3\2\2\2\u0156"+
		"\u0155\3\2\2\2\u0157G\3\2\2\2\u0158\u015c\7\f\2\2\u0159\u015b\5N(\2\u015a"+
		"\u0159\3\2\2\2\u015b\u015e\3\2\2\2\u015c\u015a\3\2\2\2\u015c\u015d\3\2"+
		"\2\2\u015dI\3\2\2\2\u015e\u015c\3\2\2\2\u015f\u0161\t\4\2\2\u0160\u015f"+
		"\3\2\2\2\u0161\u0162\3\2\2\2\u0162\u0160\3\2\2\2\u0162\u0163\3\2\2\2\u0163"+
		"K\3\2\2\2\u0164\u0166\t\5\2\2\u0165\u0164\3\2\2\2\u0166\u0167\3\2\2\2"+
		"\u0167\u0165\3\2\2\2\u0167\u0168\3\2\2\2\u0168M\3\2\2\2\u0169\u016b\7"+
		"\25\2\2\u016a\u0169\3\2\2\2\u016b\u016c\3\2\2\2\u016c\u016a\3\2\2\2\u016c"+
		"\u016d\3\2\2\2\u016d\u0174\3\2\2\2\u016e\u0174\7\37\2\2\u016f\u0174\7"+
		"\31\2\2\u0170\u0174\7\35\2\2\u0171\u0174\7#\2\2\u0172\u0174\5L\'\2\u0173"+
		"\u016a\3\2\2\2\u0173\u016e\3\2\2\2\u0173\u016f\3\2\2\2\u0173\u0170\3\2"+
		"\2\2\u0173\u0171\3\2\2\2\u0173\u0172\3\2\2\2\u0174O\3\2\2\2-X_hnv\u0082"+
		"\u0088\u0092\u009c\u00a6\u00b0\u00ba\u00c2\u00c8\u00cf\u00d4\u00da\u00e1"+
		"\u00e6\u00eb\u00f1\u00f5\u00fc\u0100\u0104\u0107\u010b\u0114\u0119\u0120"+
		"\u0123\u0128\u012e\u0134\u0140\u0148\u0150\u0156\u015c\u0162\u0167\u016c"+
		"\u0173";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}