// Generated from ABCMusic.g4 by ANTLR 4.0

package grammar;

import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface ABCMusicListener extends ParseTreeListener {
	void enterLineender(ABCMusicParser.LineenderContext ctx);
	void exitLineender(ABCMusicParser.LineenderContext ctx);

	void enterOtherfields(ABCMusicParser.OtherfieldsContext ctx);
	void exitOtherfields(ABCMusicParser.OtherfieldsContext ctx);

	void enterLyrictext(ABCMusicParser.LyrictextContext ctx);
	void exitLyrictext(ABCMusicParser.LyrictextContext ctx);

	void enterAbctune(ABCMusicParser.AbctuneContext ctx);
	void exitAbctune(ABCMusicParser.AbctuneContext ctx);

	void enterTempo(ABCMusicParser.TempoContext ctx);
	void exitTempo(ABCMusicParser.TempoContext ctx);

	void enterMidtunefield(ABCMusicParser.MidtunefieldContext ctx);
	void exitMidtunefield(ABCMusicParser.MidtunefieldContext ctx);

	void enterChord(ABCMusicParser.ChordContext ctx);
	void exitChord(ABCMusicParser.ChordContext ctx);

	void enterFieldcomposer(ABCMusicParser.FieldcomposerContext ctx);
	void exitFieldcomposer(ABCMusicParser.FieldcomposerContext ctx);

	void enterAbcline(ABCMusicParser.AbclineContext ctx);
	void exitAbcline(ABCMusicParser.AbclineContext ctx);

	void enterAbcmusic(ABCMusicParser.AbcmusicContext ctx);
	void exitAbcmusic(ABCMusicParser.AbcmusicContext ctx);

	void enterFieldvoice(ABCMusicParser.FieldvoiceContext ctx);
	void exitFieldvoice(ABCMusicParser.FieldvoiceContext ctx);

	void enterMeter(ABCMusicParser.MeterContext ctx);
	void exitMeter(ABCMusicParser.MeterContext ctx);

	void enterFieldmeter(ABCMusicParser.FieldmeterContext ctx);
	void exitFieldmeter(ABCMusicParser.FieldmeterContext ctx);

	void enterNotelengthstrict(ABCMusicParser.NotelengthstrictContext ctx);
	void exitNotelengthstrict(ABCMusicParser.NotelengthstrictContext ctx);

	void enterNoteelement(ABCMusicParser.NoteelementContext ctx);
	void exitNoteelement(ABCMusicParser.NoteelementContext ctx);

	void enterAccidental(ABCMusicParser.AccidentalContext ctx);
	void exitAccidental(ABCMusicParser.AccidentalContext ctx);

	void enterFieldtempo(ABCMusicParser.FieldtempoContext ctx);
	void exitFieldtempo(ABCMusicParser.FieldtempoContext ctx);

	void enterMeterfraction(ABCMusicParser.MeterfractionContext ctx);
	void exitMeterfraction(ABCMusicParser.MeterfractionContext ctx);

	void enterKey(ABCMusicParser.KeyContext ctx);
	void exitKey(ABCMusicParser.KeyContext ctx);

	void enterNote(ABCMusicParser.NoteContext ctx);
	void exitNote(ABCMusicParser.NoteContext ctx);

	void enterLyricline(ABCMusicParser.LyriclineContext ctx);
	void exitLyricline(ABCMusicParser.LyriclineContext ctx);

	void enterKeynote(ABCMusicParser.KeynoteContext ctx);
	void exitKeynote(ABCMusicParser.KeynoteContext ctx);

	void enterElement(ABCMusicParser.ElementContext ctx);
	void exitElement(ABCMusicParser.ElementContext ctx);

	void enterText(ABCMusicParser.TextContext ctx);
	void exitText(ABCMusicParser.TextContext ctx);

	void enterModeminor(ABCMusicParser.ModeminorContext ctx);
	void exitModeminor(ABCMusicParser.ModeminorContext ctx);

	void enterFieldtitle(ABCMusicParser.FieldtitleContext ctx);
	void exitFieldtitle(ABCMusicParser.FieldtitleContext ctx);

	void enterFieldnumber(ABCMusicParser.FieldnumberContext ctx);
	void exitFieldnumber(ABCMusicParser.FieldnumberContext ctx);

	void enterBarline(ABCMusicParser.BarlineContext ctx);
	void exitBarline(ABCMusicParser.BarlineContext ctx);

	void enterLyric(ABCMusicParser.LyricContext ctx);
	void exitLyric(ABCMusicParser.LyricContext ctx);

	void enterNotelength(ABCMusicParser.NotelengthContext ctx);
	void exitNotelength(ABCMusicParser.NotelengthContext ctx);

	void enterNthrepeat(ABCMusicParser.NthrepeatContext ctx);
	void exitNthrepeat(ABCMusicParser.NthrepeatContext ctx);

	void enterFieldkey(ABCMusicParser.FieldkeyContext ctx);
	void exitFieldkey(ABCMusicParser.FieldkeyContext ctx);

	void enterAbcheader(ABCMusicParser.AbcheaderContext ctx);
	void exitAbcheader(ABCMusicParser.AbcheaderContext ctx);

	void enterTupletspec(ABCMusicParser.TupletspecContext ctx);
	void exitTupletspec(ABCMusicParser.TupletspecContext ctx);

	void enterTupletelement(ABCMusicParser.TupletelementContext ctx);
	void exitTupletelement(ABCMusicParser.TupletelementContext ctx);

	void enterFielddefaultlength(ABCMusicParser.FielddefaultlengthContext ctx);
	void exitFielddefaultlength(ABCMusicParser.FielddefaultlengthContext ctx);

	void enterKeyaccidental(ABCMusicParser.KeyaccidentalContext ctx);
	void exitKeyaccidental(ABCMusicParser.KeyaccidentalContext ctx);

	void enterComment(ABCMusicParser.CommentContext ctx);
	void exitComment(ABCMusicParser.CommentContext ctx);

	void enterPitch(ABCMusicParser.PitchContext ctx);
	void exitPitch(ABCMusicParser.PitchContext ctx);
}