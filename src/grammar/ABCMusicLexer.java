// Generated from ABCMusic.g4 by ANTLR 4.0

package grammar;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ABCMusicLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__13=1, T__12=2, T__11=3, T__10=4, T__9=5, T__8=6, T__7=7, T__6=8, T__5=9, 
		T__4=10, T__3=11, T__2=12, T__1=13, T__0=14, KEYSHARP=15, DIGIT=16, OCTAVE=17, 
		ALPHABET=18, SPACE=19, DOT=20, EXCLAMATION=21, QUESTION=22, UNDERSCORE=23, 
		CARET=24, EQUALS=25, SLASH=26, ASTERISK=27, TILDE=28, DASH=29, SLASHDASH=30, 
		NEWLINE=31, COMMON=32, PIPE=33, REPEAT_START=34, REPEAT_END=35, BRACKETPIPE=36, 
		PIPEBRACKET=37, OPENPAREN=38, CLOSEPAREN=39;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"']'", "'%'", "'L:'", "'C:'", "'['", "'X:'", "'V:'", "'T:'", "'Q:'", "'w:'", 
		"'K:'", "'[1'", "'M:'", "'[2'", "'#'", "DIGIT", "OCTAVE", "ALPHABET", 
		"' '", "'.'", "'!'", "'?'", "'_'", "'^'", "'='", "'/'", "'*'", "'~'", 
		"'-'", "'-'", "NEWLINE", "'C|'", "'|'", "'|:'", "':|'", "'[|'", "'|]'", 
		"'('", "')'"
	};
	public static final String[] ruleNames = {
		"T__13", "T__12", "T__11", "T__10", "T__9", "T__8", "T__7", "T__6", "T__5", 
		"T__4", "T__3", "T__2", "T__1", "T__0", "KEYSHARP", "DIGIT", "OCTAVE", 
		"ALPHABET", "SPACE", "DOT", "EXCLAMATION", "QUESTION", "UNDERSCORE", "CARET", 
		"EQUALS", "SLASH", "ASTERISK", "TILDE", "DASH", "SLASHDASH", "NEWLINE", 
		"COMMON", "PIPE", "REPEAT_START", "REPEAT_END", "BRACKETPIPE", "PIPEBRACKET", 
		"OPENPAREN", "CLOSEPAREN"
	};


	    // This method makes the lexer or parser stop running if it encounters
	    // invalid input and throw a RuntimeException.
	    public void reportErrorsAsExceptions() {
	        removeErrorListeners();
	        addErrorListener(new ExceptionThrowingErrorListener());
	    }

	    private static class ExceptionThrowingErrorListener extends BaseErrorListener {
	        @Override
	        public void syntaxError(Recognizer<?, ?> recognizer,
	                Object offendingSymbol, int line, int charPositionInLine,
	                String msg, RecognitionException e) {
	            throw new RuntimeException(msg);
	        }
	    }


	public ABCMusicLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "ABCMusic.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\2\4)\u00bd\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t"+
		"\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20"+
		"\t\20\4\21\t\21\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27"+
		"\t\27\4\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36"+
		"\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4"+
		"(\t(\3\2\3\2\3\3\3\3\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6\3\7\3\7\3\7\3\b\3"+
		"\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r\3\r"+
		"\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\21\3\21\3\22\6\22~\n\22\r\22"+
		"\16\22\177\3\22\6\22\u0083\n\22\r\22\16\22\u0084\5\22\u0087\n\22\3\23"+
		"\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3\27\3\30\3\30\3\31\3\31\3\32"+
		"\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37\3\37\3\37\3 \5 \u00a5"+
		"\n \3 \3 \3!\3!\3!\3\"\3\"\3#\3#\3#\3$\3$\3$\3%\3%\3%\3&\3&\3&\3\'\3\'"+
		"\3(\3(\2)\3\3\1\5\4\1\7\5\1\t\6\1\13\7\1\r\b\1\17\t\1\21\n\1\23\13\1\25"+
		"\f\1\27\r\1\31\16\1\33\17\1\35\20\1\37\21\1!\22\1#\23\1%\24\1\'\25\1)"+
		"\26\1+\27\1-\30\1/\31\1\61\32\1\63\33\1\65\34\1\67\35\19\36\1;\37\1= "+
		"\1?!\1A\"\1C#\1E$\1G%\1I&\1K\'\1M(\1O)\1\3\2\4\3\62;\4C\\c|\u00c0\2\3"+
		"\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2"+
		"\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31"+
		"\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2"+
		"\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2"+
		"\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2"+
		"\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2"+
		"I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\3Q\3\2\2\2\5S\3\2\2\2\7U\3"+
		"\2\2\2\tX\3\2\2\2\13[\3\2\2\2\r]\3\2\2\2\17`\3\2\2\2\21c\3\2\2\2\23f\3"+
		"\2\2\2\25i\3\2\2\2\27l\3\2\2\2\31o\3\2\2\2\33r\3\2\2\2\35u\3\2\2\2\37"+
		"x\3\2\2\2!z\3\2\2\2#\u0086\3\2\2\2%\u0088\3\2\2\2\'\u008a\3\2\2\2)\u008c"+
		"\3\2\2\2+\u008e\3\2\2\2-\u0090\3\2\2\2/\u0092\3\2\2\2\61\u0094\3\2\2\2"+
		"\63\u0096\3\2\2\2\65\u0098\3\2\2\2\67\u009a\3\2\2\29\u009c\3\2\2\2;\u009e"+
		"\3\2\2\2=\u00a0\3\2\2\2?\u00a4\3\2\2\2A\u00a8\3\2\2\2C\u00ab\3\2\2\2E"+
		"\u00ad\3\2\2\2G\u00b0\3\2\2\2I\u00b3\3\2\2\2K\u00b6\3\2\2\2M\u00b9\3\2"+
		"\2\2O\u00bb\3\2\2\2QR\7_\2\2R\4\3\2\2\2ST\7\'\2\2T\6\3\2\2\2UV\7N\2\2"+
		"VW\7<\2\2W\b\3\2\2\2XY\7E\2\2YZ\7<\2\2Z\n\3\2\2\2[\\\7]\2\2\\\f\3\2\2"+
		"\2]^\7Z\2\2^_\7<\2\2_\16\3\2\2\2`a\7X\2\2ab\7<\2\2b\20\3\2\2\2cd\7V\2"+
		"\2de\7<\2\2e\22\3\2\2\2fg\7S\2\2gh\7<\2\2h\24\3\2\2\2ij\7y\2\2jk\7<\2"+
		"\2k\26\3\2\2\2lm\7M\2\2mn\7<\2\2n\30\3\2\2\2op\7]\2\2pq\7\63\2\2q\32\3"+
		"\2\2\2rs\7O\2\2st\7<\2\2t\34\3\2\2\2uv\7]\2\2vw\7\64\2\2w\36\3\2\2\2x"+
		"y\7%\2\2y \3\2\2\2z{\t\2\2\2{\"\3\2\2\2|~\7)\2\2}|\3\2\2\2~\177\3\2\2"+
		"\2\177}\3\2\2\2\177\u0080\3\2\2\2\u0080\u0087\3\2\2\2\u0081\u0083\7.\2"+
		"\2\u0082\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0085"+
		"\3\2\2\2\u0085\u0087\3\2\2\2\u0086}\3\2\2\2\u0086\u0082\3\2\2\2\u0087"+
		"$\3\2\2\2\u0088\u0089\t\3\2\2\u0089&\3\2\2\2\u008a\u008b\7\"\2\2\u008b"+
		"(\3\2\2\2\u008c\u008d\7\60\2\2\u008d*\3\2\2\2\u008e\u008f\7#\2\2\u008f"+
		",\3\2\2\2\u0090\u0091\7A\2\2\u0091.\3\2\2\2\u0092\u0093\7a\2\2\u0093\60"+
		"\3\2\2\2\u0094\u0095\7`\2\2\u0095\62\3\2\2\2\u0096\u0097\7?\2\2\u0097"+
		"\64\3\2\2\2\u0098\u0099\7\61\2\2\u0099\66\3\2\2\2\u009a\u009b\7,\2\2\u009b"+
		"8\3\2\2\2\u009c\u009d\7\u0080\2\2\u009d:\3\2\2\2\u009e\u009f\7/\2\2\u009f"+
		"<\3\2\2\2\u00a0\u00a1\7^\2\2\u00a1\u00a2\7/\2\2\u00a2>\3\2\2\2\u00a3\u00a5"+
		"\7\17\2\2\u00a4\u00a3\3\2\2\2\u00a4\u00a5\3\2\2\2\u00a5\u00a6\3\2\2\2"+
		"\u00a6\u00a7\7\f\2\2\u00a7@\3\2\2\2\u00a8\u00a9\7E\2\2\u00a9\u00aa\7~"+
		"\2\2\u00aaB\3\2\2\2\u00ab\u00ac\7~\2\2\u00acD\3\2\2\2\u00ad\u00ae\7~\2"+
		"\2\u00ae\u00af\7<\2\2\u00afF\3\2\2\2\u00b0\u00b1\7<\2\2\u00b1\u00b2\7"+
		"~\2\2\u00b2H\3\2\2\2\u00b3\u00b4\7]\2\2\u00b4\u00b5\7~\2\2\u00b5J\3\2"+
		"\2\2\u00b6\u00b7\7~\2\2\u00b7\u00b8\7_\2\2\u00b8L\3\2\2\2\u00b9\u00ba"+
		"\7*\2\2\u00baN\3\2\2\2\u00bb\u00bc\7+\2\2\u00bcP\3\2\2\2\7\2\177\u0084"+
		"\u0086\u00a4";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}