package sound;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RestTest {

	/**
	 * This test tests that the Rest datatype correctly functions for all inputs.
	 * Input patitions: 0, 1, max double, min double.
	 */
    @Test
    public void testDuration(){
        Rest r1 = new Rest(1);
        assertEquals(true, 1 == r1.duration());
        Rest r0 = new Rest(0);
        assertEquals(true, 0 == r0.duration());
        Rest rMax = new Rest(Double.MAX_VALUE);
        assertEquals(true, Double.MAX_VALUE == rMax.duration());
        Rest rMin = new Rest(Double.MIN_VALUE);
        assertEquals(true, Double.MIN_VALUE == rMin.duration());
    }
}