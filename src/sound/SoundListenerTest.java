package sound;

import grammar.ABCMusicLexer;
import grammar.ABCMusicParser;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Test;

/**
 * @category no_didit
 * @author Johnathon Root
 * 
 */
public class SoundListenerTest {
	/*
	 * The following is a test of the SoundListener class. This test is
	 * considered an end-to-end test, as each element (our classes, the lexer,
	 * the SequencePlayer), are tested separately. Each abc file played tests
	 * certain parts of the implementation.
	 * 
	 * Due to limitations in our understanding of Java (or, perhaps, limitations
	 * in Java), the filenames for this test must be changed on each computer
	 * that runs this test.
	 */

	@Test
	public void testPieces() {
		// Piece 1's critical test is that our triplets work as advertised in
		// the 3rd measure. It also tests lyrics.
		play("C:/Users/Johnathon Root/abcplayer/sample_abc/abc_song.abc");
		// Piece 2 works to test chords, accidentals, and triplets.
		play("C:/Users/Johnathon Root/abcplayer/sample_abc/piece2.abc");
		// Piece 3 tests lyrics.
		play("C:/Users/Johnathon Root/abcplayer/sample_abc/piece3.abc");
		// test.abc is a handwritten test abc file that tests a tuple, lyrics,
		// and 1st/2nd endings.
		play("C:/Users/Johnathon Root/abcplayer/sample_abc/test.abc");
		// Invention no. 1 tests our voicing.
		play("C:/Users/Johnathon Root/abcplayer/sample_abc/invention.abc");
		// Fur Elise tests our repeats. This test can be stopped early if the
		// first repeat doesn't work or if you don't want to hear all of Fur
		// Elise.
		play("C:/Users/Johnathon Root/abcplayer/sample_abc/fur_elise.abc");
	}

	public static void play(String file) {

		try {
			String fileContents = readFile(file);

			// Generate the stream of tokens from the lexer
			CharStream stream = new ANTLRInputStream(fileContents);
			ABCMusicLexer lexer = new ABCMusicLexer(stream);
			lexer.reportErrorsAsExceptions();
			TokenStream tokens = new CommonTokenStream(lexer);

			// Send the tokens to the parser
			ABCMusicParser parser = new ABCMusicParser(tokens);
			parser.reportErrorsAsExceptions();

			// Create the parse tree using the starter rule, abctune
			ParseTree tree = parser.abctune();

			// Walk the parse tree to generate the Song object
			ParseTreeWalker walker = new ParseTreeWalker();
			SoundListener listener = new SoundListener();
			walker.walk(listener, tree);

			// Retrieve the Song object from the listener, and play it!
			Song song = listener.getSong();
			song.play();

		} catch (IOException e) {

			// no such file!
			System.err.println("Failure. That file doesn't seem to exist!");
		}

	}

	/**
	 * Read a file to a String.
	 * 
	 * @param filename
	 *            The file to open.
	 * @return A String containing the contents of the file.
	 * @throws IOException
	 *             If the file cannot be opened.
	 */
	public static String readFile(String filename) throws IOException {
		// read in the file directly as a byte array
		byte[] byteArray = Files.readAllBytes(Paths.get(filename));
		// decode that byte array using the default charset so it's just text
		return Charset.defaultCharset().decode(ByteBuffer.wrap(byteArray))
				.toString();
	}
}