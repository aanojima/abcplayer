package sound;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * This class is for testing the methods of the Note class.
 */
public class NoteTest {
    
    /* getNote Tests
     * 
     * These will simply make sure the proper note is returned, testing the partitions:
     * - The note is uppercase
     * - The note is lowercase
     * - The note is plain
     * - The note has an accidental
     * - The note has an octave marker
     * - The note has an accidental and octave marker
     * - The note has a lot of decorations.
     */
    
    @Test
    public void test_getNote_Plain() {
        Note note = new Note("C", 2);
        assertEquals('C', note.getNote());
    }
    
    @Test
    public void test_getNote_Accidental() {
        Note note = new Note("^d", 3);
        assertEquals('D', note.getNote());
    }
    
    @Test
    public void test_getNote_Octave() {
        Note note = new Note("F,", 0.5);
        assertEquals('F', note.getNote());
    }
    
    @Test
    public void test_getNote_AccidentalAndOctave() {
        Note note = new Note("_e'", 1);
        assertEquals('E', note.getNote());
    }
    
    @Test
    public void test_getNote_VeryDecorated() {
        Note note = new Note("^^=_=B,',''", 0.25);
        assertEquals('B', note.getNote());
    }
    
    /* getAccidental Tests
     * 
     * These will simply make sure the proper accidental value is returned, testing the partitions:
     * - The note is plain
     * - The note has an accidental
     * - The note has multiple accidentals
     */
    
    @Test
    public void test_getAccidental_Plain() {
        Note note = new Note("a", 2);
        assertEquals(0, note.getAccidental());
    }
    
    @Test
    public void test_getAccidental_1Accidental() {
        Note note = new Note("^d", 3);
        assertEquals(1, note.getAccidental());
    }
    
    @Test
    public void test_getAccidental_MultipleAccidentals() {
        Note note = new Note("_^^_=_F,", 0.5);
        assertEquals(-1, note.getAccidental());
    }
    
    /* getOctave Tests
     * 
     * These will simply make sure the proper octave value is returned, testing the partitions:
     * - The note is plain
     * - The note has an octave marker
     * - The note has multiple octave markers
     */
    
    @Test
    public void test_getOctave_Plain() {
        Note note = new Note("G", 2);
        assertEquals(0, note.getOctave());
    }
    
    @Test
    public void test_getOctave_1Marker() {
        Note note = new Note("^=D,", 3);
        assertEquals(-1, note.getOctave());
    }
    
    @Test
    public void test_getOctave_MultipleMarkers() {
        Note note = new Note("_^^_=_F,''", 0.5);
        assertEquals(1, note.getOctave());
    }
    
    // These two tests are fairly straightforward, testing addLyric, getLyric, and duration
    
    @Test
    public void test_addAndGetLyric() {
        Note note = new Note("e'", 2);
        note.addLyric("Lyrics!");
        assertEquals("Lyrics!", note.getLyric());
    }
    
    @Test
    public void test_duration() {
        Note note = new Note("=D,", 0.25);
        assertEquals(true, 0.25 == note.duration());
    }
    
    /* equals Tests
     * 
     * We will make sure equal notes are deemed equal by the equals method, using the partitions:
     * - The note is uppercase
     * - The note is lowercase
     * - The note is plain
     * - The note has an accidental
     * - The note has an octave marker
     * - The note has an accidental and octave marker
     * - The note has many decorations
     * 
     * We will also make sure notes with different representations ut the same value are equal.
     */
    
    @Test
    public void test_equals_Plain() {
        Note note = new Note("C", 2);
        Note note2 = new Note("C", 2);
        assertEquals(note, note2);
    }
    
    @Test
    public void test_equals_Accidental() {
        Note note = new Note("^d", 3);
        Note note2 = new Note("^d", 3);
        assertEquals(note, note2);
    }
    
    @Test
    public void test_equals_Octave() {
        Note note = new Note("F,", 0.5);
        Note note2 = new Note("F,", 1.0/2);
        assertEquals(note, note2);
    }
    
    @Test
    public void test_equals_AccidentalAndOctave() {
        Note note = new Note("_e'", 1);
        Note note2 = new Note("_e'", 1.0);
        assertEquals(note, note2);
    }
    
    @Test
    public void test_equals_VeryDecorated() {
        Note note = new Note("^=^=_B,',''", 0.25);
        Note note2 = new Note("^b", 0.25);
        assertEquals(note, note2);
    }

}
