package sound;

public class Rest implements MusicElement {

	private final double duration;

	/**
	 * 
	 * @param duration: a double greater than zero representing a fraction of a beat.  
     * This determines how long the rest last for. 
	 */
	public Rest(double duration) {
		this.duration = duration;
	}

	@Override
	public double duration() {
		return this.duration;
	}
	
	@Override
	public void addSequence(SequencePlayer sequence, double startTime, int ticksPerBeat) {
		// This is a rest. Don't sequence anything
	}

}
