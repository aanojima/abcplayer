package sound;

import java.util.ArrayList;
import java.util.Hashtable;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

/**
 * The Song class stores information about a song, including title, composer,
 * and MIDI information contained as lists of MusicElements.
 */
public class Song {

	// This hashtable maps a track name to a list of MusicElements,
	// storing the music of the different voices of the song.
	private Hashtable<String, ArrayList<MusicElement>> tracks;

	// Internally, we use 12 ticks per quarter note, counting a quarter note as
	// a beat.
	// The BPM is appropriately calculated in the SoundListener, or whatever
	// creates the Song.
	private final int TICKS_PER_BEAT = 12;

	// The BPM of the song, which is actually the number of quarter notes per
	// minute.
	private int bpm;

	// The LyricListener simply prints each new lyric on a new line.
	private LyricListener lyricListener = new LyricListener() {
		public void processLyricEvent(String text) {
			if (!text.equals("")) {
				System.out.println(text);
			}
		}
	};

	// Fields for the title and composer.
	private String title;
	private String composer;

	/**
	 * Constructs a new Song instance.
	 * 
	 * @param tracks A Hashtable mapping track names to lists of MusicElements
	 * @param bpm The BPM of the song, actually in quarter notes per measure
	 * @param title The title of the song
	 * @param composer The composer of the song
	 */
	public Song(Hashtable<String, ArrayList<MusicElement>> tracks, int bpm,
			String title, String composer) {
		this.tracks = tracks;
		this.bpm = bpm;
		this.title = title;
		this.composer = composer;
	}

	/**
	 * Gets a SequencePlayer containing the MIDI data of this song. This player
	 * can then be played to hear the song.
	 * 
	 * @return A SequencePlayer object containing the MIDI data of this song.
	 * @throws MidiUnavailableException
	 * @throws InvalidMidiDataException
	 */
	public SequencePlayer getPlayer() throws MidiUnavailableException,
			InvalidMidiDataException {

		SequencePlayer sequence = new SequencePlayer(this.bpm,
				this.TICKS_PER_BEAT, this.lyricListener);

		for (String trackName : tracks.keySet()) {
			ArrayList<MusicElement> elements = tracks.get(trackName);
			double startTime = 0;
			for (int i = 0; i < elements.size(); i++) {
				MusicElement currentElement = elements.get(i);
				currentElement.addSequence(sequence, startTime,
						this.TICKS_PER_BEAT);
				startTime += currentElement.duration();
			}
		}

		return sequence;
	}

	/**
	 * Plays this song, or prints a message if an error occurs.
	 */
	public void play() {
		// This function catches all exceptions itself so it's easier to use.
		try {
			SequencePlayer player = this.getPlayer();

			System.out.println("Title: " + this.title);
			System.out.println("Composer: " + this.composer);

			player.play();
		} catch (MidiUnavailableException | InvalidMidiDataException e) {
			System.err.println("Oh no, something's wrong. Is MIDI available?");
		}
	}

}
