package sound;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiUnavailableException;

import org.junit.Test;

/**
 * This is for testing that the MusicElements can be sequenced and played.
 * @category no_didit
 */
public class MusicElementSequenceTest {
    
    /* This tests the addSequence function of MusicElements,
     * making sure they can be sequenced and played
     */
    @Test
    public void testSequencePlayer() {
        
        SequencePlayer player;
        try {

            // Create a new player, with 120 beats per minute, 12 ticks per beat
            // and a LyricListener that prints each lyric that it sees.
            LyricListener listener = new LyricListener() {
                public void processLyricEvent(String text) {
                    if(!text.equals("")) {
                        System.out.println(text);
                    }
                }
            };
            player = new SequencePlayer(120, 12, listener);

            // create an array of MusicElements which we will add to the sequencer
            MusicElement[] noteArray = new MusicElement[10];
            noteArray[0] = new Note("C", 1);
            noteArray[1] = new Note("D", 1);
            noteArray[2] = new Note("E", 1);
            noteArray[3] = new Note("F", 1);
            noteArray[4] = new Note("G", 1);
            noteArray[5] = new Note("A", 1);
            noteArray[6] = new Note("B", 1);
            noteArray[7] = new Note("c", 1);
            noteArray[8] = new Rest(1);
            noteArray[9] = new Chord(new Note[] { new Note("C", 1), 
                    new Note("E", 1), 
                    new Note("G", 1) }, 2);

            int startTime = 0;
            for (int i = 0; i < noteArray.length; i++) {
                noteArray[i].addSequence(player, startTime, 12);
                startTime += noteArray[i].duration();
            }

            System.out.println(player);

            // play!
            player.play();

            /*
             * Note: A possible weird behavior of the Java sequencer: Even if
             * the sequencer has finished playing all of the scheduled notes and
             * is manually closed, the program may not terminate. This is likely
             * due to daemon threads that are spawned when the sequencer is
             * opened but keep on running even after the sequencer is killed. In
             * this case, you need to explicitly exit the program with
             * System.exit(0).
             */
            // System.exit(0);

        } catch (MidiUnavailableException e) {
            e.printStackTrace();
        } catch (InvalidMidiDataException e) {
            e.printStackTrace();
        }
        
    }

}
