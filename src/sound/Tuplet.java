package sound;

public class Tuplet implements MusicElement, LyricElement {

	private final MusicElement[] elements;
	// the ratio of beats/note, determines how long to play each note
	private final double ratio;

	/**
	 * Creates a Tuplet instance.
	 * 
	 * @param elements: an array of music elements, in chronological
	 * order, that the tuplet should play.  Can be notes, chords, and
	 * (even though it's weird) rests.
	 * 
	 * @param ratio: the time length (a double greater than zero 
	 * in ratio of beats per note) each music element in the tuplet 
	 * should play for.  
	 */
	public Tuplet(MusicElement[] elements, double ratio) {
		this.elements = elements;
		this.ratio = ratio;
	}
	
	/**
	 * Gets the number of elements in the tuplet.
	 * @return An integer greater than zero representing the tuplet size
	 */
	public int size() {
	    return this.elements.length;
	}
	
	@Override
	public void addLyric(String text) {
	    for(int i = 0; i < this.elements.length; i++) {
	        LyricElement currentElement = (LyricElement) this.elements[i];
	        if(currentElement.getLyric().equals("")) {
	            // this element does not already have a lyric, so add it and stop
	            currentElement.addLyric(text);
	            break;
	        }
	    }
	}
	
	@Override
	public String getLyric() {
	    String lyrics = "";
	    for(int i = 0; i < this.elements.length; i++) {
	        LyricElement currentElement = (LyricElement) this.elements[i];	        
	        if (i == this.elements.length - 1){
	            // Add element lyrics to tuplet lyrics without space if last element
	            lyrics += currentElement.getLyric();
	        } else {
	            // Add space inbetween element lyrics and add to tuplet lyrics
	            lyrics += currentElement.getLyric()+" ";
	        }
	        
	    }
	    
	    // Get rid of extraneous whitespace
	    lyrics = lyrics.trim();

	    return lyrics;
	}

	@Override
	public double duration() {
		double duration = 0;
		for (int i = 0; i < this.elements.length; i++) {
			// Convert each element duration using ratio
		    duration += this.elements[i].duration() * ratio;
		}
		return duration;
	}

	@Override
	public void addSequence(SequencePlayer sequence, double startTime, int ticksPerBeat) {
		int startTick = (int) (startTime * ticksPerBeat);
		for (int i = 0; i < this.elements.length; i++) {
			MusicElement element = this.elements[i];
			// Convert each element duration using ratio and ticks per beat
			int duration = (int) (element.duration() * ratio * ticksPerBeat);
			if (element instanceof Note) {
			    // NOTES
				Note note = (Note) element;
				sequence.addNote(new Pitch(note.getNote(),
						note.getAccidental(), note.getOctave()).toMidiNote(),
						startTick, duration);
				// Lyrics
				sequence.addLyricEvent(note.getLyric(), startTick);
			} else if (element instanceof Chord) { 
			    // CHORDS
				Chord chord = (Chord) element;
				Note[] notes = chord.getNotes();
				// Each Note in Chord
				for (int j = 0; j < notes.length; j++) {
					sequence.addNote(
							new Pitch(notes[j].getNote(),
							        notes[j].getAccidental(),
							        notes[j].getOctave()).toMidiNote(),
							startTick, duration);
				}
				// Lyrics
				sequence.addLyricEvent(chord.getLyric(), startTick);
			} else {
				// Rests don't have lyrics
			    throw new RuntimeException("Problem: Encountered a rest in a tuplet.");
			}
			// Next element will start immediately following the previous element
			startTick += duration;
		}

	}

}
