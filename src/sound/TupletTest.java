package sound;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TupletTest {

    /*
     * We test to make sure we can add lyrics to a Tuplet,
     * get the size of the tuplet, and get its ratio
     */
    
    @Test
    public void testTupletSize(){
        // Case I: Regular 2-sized Tuplet with Note and Chord
        Tuplet tuplet = new Tuplet(new MusicElement[] {new Note("C", 1), 
            new Chord(new Note[] {
                        new Note("C", 1),
                        new Note("E", 1),
                        new Note("G", 1)}, 
                      1)
                                                      }, 1);
        assertEquals(2, tuplet.size());
        // Case II: Empty Tuplet
        Tuplet tuplet2 = new Tuplet(new MusicElement[] {}, 1);
        assertEquals(0, tuplet2.size());
    }
    
    @Test
    public void testAddLyrics(){
        
        // Case I: Each MusicElement already has lyric
        Note note1 = new Note("C", 1);
        note1.addLyric("Hello");
        Chord chord1 = new Chord(new Note[] {new Note("C", 1), new Note("E", 1), new Note("G", 1)}, 1);
        chord1.addLyric("World!");
        Tuplet tuplet1 = new Tuplet(new MusicElement[] {note1, chord1}, 2);
        tuplet1.addLyric("This is bad if you can see me!");
        assertEquals("Hello World!", tuplet1.getLyric());
        
        // Case II: Some MusicElements already have a lyric
        Note note2 = new Note("C", 1);
        note2.addLyric("Hello!");
        Chord chord2 = new Chord(new Note[] {new Note("C", 1), new Note("E", 1), new Note("G", 1)}, 1);
        chord2.addLyric("");
        Tuplet tuplet2 = new Tuplet(new MusicElement[] {note2, chord2}, 2);
        tuplet2.addLyric("This time I hope you can see me!");
        assertEquals("Hello! This time I hope you can see me!", tuplet2.getLyric());        
        
        // Case III: Tuplet as a whole has one lyric
        Tuplet tuplet3 = new Tuplet(new Note[] { new Note("C", 1), new Note("E", 1), new Note("G", 1) }, 2);
        tuplet3.addLyric("Yes I can");
        assertEquals("Yes I can", tuplet3.getLyric());                        
        
        // Case IV: Empty Lyric added
        Tuplet tuplet4 = new Tuplet(new Note[] { new Note("C_", 3), new Note("E_", 3), new Note("G_", 3) }, 3);
        tuplet4.addLyric("");
        assertEquals("", tuplet4.getLyric());
        
        // Case V: Foreign Languages and Other Symbols
        Tuplet tuplet5 = new Tuplet(new Note[] { new Note("c_", 3), new Note("e_", 3), new Note("g_", 3) }, 3);
        tuplet5.addLyric("HASHTAG_#花は咲く, HASHTAG_#OMGIMMASOJAPANESE(:3)");
        assertEquals("HASHTAG_#花は咲く, HASHTAG_#OMGIMMASOJAPANESE(:3)", tuplet5.getLyric());
        
        // Case VI: Add Multiple Lyrics to Tuplet
        Tuplet tuplet6 = new Tuplet(new Note[] { new Note("C", 3), new Note("E", 3), new Note("G", 3)}, 3);
        tuplet6.addLyric("Noj");
        tuplet6.addLyric("Schmidt");
        tuplet6.addLyric("Root");
        tuplet6.addLyric("INVISIBLE");
        assertEquals("Noj Schmidt Root", tuplet6.getLyric());
        
        
        // 
    }
    
    @Test
    public void testGetDuration(){
        
        // All Notes
        Tuplet tuplet = new Tuplet(new MusicElement[] { new Note("A", 1), new Note("C", 2), new Note("E", 3) }, 1.5);
        assertEquals(true, 9 == tuplet.duration());
        
        // All Chords
        Chord chordA = new Chord(new Note[] {new Note("C", 1), new Note("E", 300)}, 2);
        Tuplet tupletA = new Tuplet(new MusicElement[] {chordA, chordA}, 2.8);
        assertEquals(true, 11.2 == tupletA.duration());
        
    }
    
}
