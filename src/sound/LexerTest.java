package sound;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.junit.Test;

import grammar.ABCMusicLexer;

public class LexerTest {
	@Test
	public void testTokens() {
		// This grammar has a lot of tokens. 25 of them, to be exact. This is a
		// test to make sure that the lexer correctly recognizes every single one of them.

		String testString = "#27',LexerTest .!?_^=/*~-\r\nC|||::|[||]()";
		String[] result = { "#", "2", "7", "'", ",", "L", "e", "x", "e", "r",
				"T", "e", "s", "t", " ", ".", "!", "?", "_", "^", "=", "/","*",
				"~", "-", "\r\n", "C|", "|", "|:", ":|", "[|", "|]", "(", ")" };
		verifyLexer(testString, result);
	}

	public void verifyLexer(String input, String[] expectedTokens) {
		CharStream stream = new ANTLRInputStream(input);
		ABCMusicLexer lexer = new ABCMusicLexer(stream);
		lexer.reportErrorsAsExceptions();
		List<? extends Token> actualTokens = lexer.getAllTokens();

		assertEquals(expectedTokens.length, actualTokens.size());

		for (int i = 0; i < actualTokens.size(); i++) {
			String actualToken = actualTokens.get(i).getText();
			String expectedToken = expectedTokens[i];
			assertEquals(actualToken, expectedToken);
		}
	}
}