package sound;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

/**
 * The GUI class simply provides a file picker for the user to choose the ABC file to play.
 */
public class GUI extends JPanel {
    
    // should no file be chosen, this is the default to play
    private final String DEFAULT_FILE = "C:/Users/LLP-admin/Documents/6.005/abcplayer/sample_abc/fur_elise.abc";
    
    // the file chooser object for picking a file
    private final JFileChooser fileChooser;
    
    public GUI() {
        
        // initialize the file chooser
        fileChooser = new JFileChooser();
        
    }
    
    /**
     * Opens a file chooser for the user to browse and find an ABC file.
     * @return The absolute filepath of the file as a String
     */
    public String chooseFile() {
        // open the file browser
        int result = fileChooser.showOpenDialog(this);
        if (result == JFileChooser.APPROVE_OPTION) {
            // a file was chosen, return its filepath
            File chosenFile = fileChooser.getSelectedFile();
            return chosenFile.getAbsolutePath();
        } else {
            // no file was chosen (box closed or canceled) so return default filepath
            return DEFAULT_FILE;
        }
    }

}
