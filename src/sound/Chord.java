package sound;

public class Chord implements MusicElement, LyricElement {

	private Note[] notes;
	private final double duration;
	private String lyric;

	/**
	 * @param notes: an array of Notes.  
	 * The duration for each note should not matter (since each will
	 * be overridden by chord duration).  
	 * @param duration: a double greater than zero representing a fraction of a beat.  
     * This determines how long the note should be played for. 
	 */
	public Chord(Note[] notes, double duration) {
		this.notes = notes.clone();
		this.duration = duration;
		// Initially set lyric blank
		this.lyric = "";
	}

	/**
     * Adds a lyric to the chord
     * @param text the lyric to be added to the chord
     */
	public void addLyric(String text) {
        this.lyric = text;
    }
	
	/**
	 * Gets the notes in the chord.  
	 * @return a clone of the notes array to avoid mutability
	 */
	public Note[] getNotes() {
		return this.notes.clone();
	}
	
	/**
     * Gets the lyric of the note.  
     * @return the string lyrical text that plays alongside the note.  
     */
	public String getLyric(){
	    return this.lyric;
	}

	@Override
	public double duration() {
		return this.duration;
	}

	@Override
	public void addSequence(SequencePlayer sequence, double startTime, int ticksPerBeat) {
	    // Converts start time into start tick based on ticks per beat
	    int startTick = (int) (startTime*ticksPerBeat);
		// Adds each note in the chord to the sequencer with duration based on chord duration
	    for (int i = 0; i < this.notes.length; i++) {
			Note note = this.notes[i];
		    sequence.addNote(new Pitch(
		                note.getNote(), 
		                note.getAccidental(), 
		                note.getOctave()).toMidiNote(), 
		            startTick,
					(int) (this.duration*ticksPerBeat));
		}
	    // Sequence the lyric
		sequence.addLyricEvent(this.lyric, startTick);
	}

}
