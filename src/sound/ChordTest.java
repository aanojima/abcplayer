package sound;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

/**
 * This suite is for testing the methods of the Chord class.
 */
public class ChordTest {
    
    /*
     * These tests are fairly straightforward; we make sure we can add lyrics to a Chord,
     * get the array of Notes in the Chord, and get its duration
     */
    
    @Test
    public void test_addAndGetLyric() {
        Chord chord = new Chord(new Note[] { new Note("C", 1), new Note("E", 1), new Note("G", 1) }, 2);
        chord.addLyric("Yes");
        assertEquals("Yes", chord.getLyric());
    }
    
    @Test
    public void test_getNotes() {
        Chord chord = new Chord(new Note[] { new Note("^C", 1), new Note("^E", 1), new Note("^G", 1) }, 2);
        Note[] notes = new Note[] {new Note("^C", 1), new Note("^E", 1), new Note("^G", 1)};
        assertArrayEquals(notes, chord.getNotes());
    }
    
    @Test
    public void test_duration() {
        Chord chord = new Chord(new Note[] { new Note("A", 1), new Note("C", 1), new Note("E", 1) }, 1.5);
        assertEquals(true, 1.5 == chord.duration());
    }

}
