package sound;

public interface MusicElement {
    
    /**
     * Gets the duration of this element.
     * @return The duration of this element, in fractions of a quarter note
     */
    public double duration();
    
    /**
     * Sequences this MusicElement by adding it to a given SequencePlayer
     * @param sequence The SequencePlayer object to add this element to
     * @param startTime The start time of the element, in number of quarter notes
     * @param ticksPerBeat The number of ticks per quarter note
     */
    public void addSequence(SequencePlayer sequence, double startTime, int ticksPerBeat);
    
}
