package sound;

public class Note implements MusicElement, LyricElement {

	private final String note;
	private final double duration;
	private String lyric;

	/**
	 * Creates a Note instance.
	 * 
	 * @param note: a string containing information about the note.  
	 * Can contain one of the letters from a to g (lower-case or upper-case),
	 * a type of accidental information characters ("=", "^", and "_"), and
	 * a type of octave information characters ("," and "'").  
	 * Ideally, there should only be one type of accidental character and/or 
	 * one type of octave character but the each can appear multiple times.  
	 * Note can incorporate mixed types of accidental and octave characters, 
	 * but that's just redundant.  
	 * 
	 * @param duration: a double greater than zero representing a fraction of a beat.  
	 * This determines how long the note should be played for.  
	 */
	public Note(String note, double duration) {
		this.duration = duration;
		this.note = note;
		// Initially leave the lyrics for this note blank 
		this.lyric = "";
	}
	
	/**
	 * Adds lyric to the note
	 * @param text a string without restrictions
	 */
	public void addLyric(String text) {
		this.lyric = text;
	}

	/**
	 * Gets the musical note of the Note.
	 * The note will be any upper-case letter from A to G.
	 * @return a character representing the musical note.
	 */
	public char getNote() {		
	    // By default if no alphabetic character is found, return "C"
	    char note = 'C';
		// Check each substring of length 1 in the note string
		String[] noteElements = this.note.split("");
		for (int i = 0; i < noteElements.length; i++) {
			String el = noteElements[i];
			// Note characters: a-g or A-G
			if (el.matches("[A-Ga-g]")) {
				// Return the character corresponding to the upper-case letter of this note
				note = el.toUpperCase().charAt(0);
			}
		}
		return note;
	}

	/**
	 * Gets the accidental value of the note from the note string. 
	 * Sharps are "^", flats are "_", and neutrals are "=".
	 * If none of the symbols are found, equivalent to a neutral note.
	 * 
	 * A negative returned integer is equivalent to the number of flats applied to the note.
	 * A positive returned  integer is equivalent to the number of sharps applied to the note.
	 * Zero is equivalent to a neutral accidental value.
	 * 
	 * @return an integer representing the accidental value of the note.  
	 */
	public int getAccidental() {
		int accidental = 0;
		String[] noteElements = this.note.split("");
		for (int i = 0; i < noteElements.length; i++) {
			String el = noteElements[i];
			if (el.equals("_")) {
				accidental--;
			} else if (el.equals("^")) {
				accidental++;
			}
		}
		return accidental;
	}

	/**
	 * Gets the octave value of the note.  
	 * @return an integer representing the octave value of the note.
	 */
	public int getOctave() {
		int octave = 0;
		String[] noteElements = this.note.split("");
		for (int i = 0; i < noteElements.length; i++) {
			String el = noteElements[i];
			if (el.matches("[ABCDEFGabcdefg]") && el.toLowerCase().equals(el)) {
				// it's a lowercase note, so add 1 to octave
				octave++;
			} else if (el.equals(",")) {
				octave--;
			} else if (el.equals("'")) {
				octave++;
			}
		}
		return octave;
	}

	/**
	 * Gets the lyric attached to the note.  
	 * @return the lyrical text that plays alongside the note.  
	 */
	public String getLyric() {
		return this.lyric;
	}

	@Override
	public double duration() {
		return this.duration;
	}

	@Override
	public void addSequence(SequencePlayer sequence, double startTime,
			int ticksPerBeat) {
		// Converts start time into start tick based on ticks per
	    int startTick = (int) (startTime * ticksPerBeat);
	    // Pitch
		sequence.addNote(
				new Pitch(this.getNote(), this.getAccidental(), this
						.getOctave()).toMidiNote(), startTick,
				(int) (this.duration * ticksPerBeat));
		// Lyric (using same time)
		sequence.addLyricEvent(this.lyric, startTick);
	}
	
	@Override
	public boolean equals(Object obj) {
	    if(!(obj instanceof Note)) {
	        return false;
	    } else {
	        Note other = (Note) obj;
	        return (this.getNote() == other.getNote()
	                && this.getAccidental() == other.getAccidental()
	                && this.getOctave() == other.getOctave()
	                && this.duration() == other.duration());
	    }
	}
	
	@Override
	public int hashCode() {
	    return this.getAccidental() + this.getOctave()*12 + (int) this.duration();
	}

}
