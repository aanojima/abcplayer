package sound;

/**
 * This interface exists to use music elements which can have lyrics
 * (Notes and Chords)
 * 
 * @author eschmidt
 */
public interface LyricElement {
    
    /**
     * Adds a lyric to this element.
     * @param text The lyric text to add
     */
    public void addLyric(String text);
    
    /**
     * Gets the lyric associated with this element.
     * @return The lyric attached to this element
     */
    public String getLyric();

}
