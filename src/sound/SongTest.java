package sound;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * This is a test for the song class. This test tests that:
 * 1. Notes, rests, chords, and tuplets are correctly added to the song.
 * 2. Tracks (or voices) function can function independently and work as indended by the abc format.
 * 3. The Song's play() function works as specified.
 * 
 * @author Eric Schmidt
 * @category no_didit
 */
public class SongTest {

    public static void main(String[] args) {
        
        ArrayList<MusicElement> testTrack = new ArrayList<MusicElement>();
        testTrack.add(new Note("C", 1));
        testTrack.add(new Note("D", 1));
        testTrack.add(new Note("E", 1));
        testTrack.add(new Note("F", 1));
        testTrack.add(new Note("G", 1));
        testTrack.add(new Note("A", 1));
        testTrack.add(new Note("B", 1));
        testTrack.add(new Note("c", 1));
        testTrack.add(new Rest(2));
        testTrack.add(new Chord(new Note[] { new Note("C", 1), new Note("E", 1), new Note("G", 1) }, 2));
        
        ArrayList<MusicElement> testTrack2 = new ArrayList<MusicElement>();
        testTrack2.add(new Note("c", 1));
        testTrack2.add(new Note("B", 1));
        testTrack2.add(new Note("A", 1));
        testTrack2.add(new Note("G", 1));
        testTrack2.add(new Note("F", 1));
        testTrack2.add(new Note("E", 1));
        testTrack2.add(new Note("D", 1));
        testTrack2.add(new Note("C", 1));
        testTrack2.add(new Rest(2));
        testTrack2.add(new Tuplet(new Note[] { new Note("c", 0.5), new Note("e", 0.5), new Note("g", 0.5) }, 2.0 / 3));
        
        Hashtable<String, ArrayList<MusicElement>> tracks = new Hashtable<String, ArrayList<MusicElement>>();
        tracks.put("track1", testTrack);
        tracks.put("track2", testTrack2);
        
        Song testSong = new Song(tracks, 120, "Test", "Eric");
        
        testSong.play();

    }

}
