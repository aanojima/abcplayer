The team contract and design documents are in this directory.

Files in this directory:
- team-contract.docx
- team-contract.pdf
- design-milestone.pdf
- design-revised.pdf

If you add any other documents to this directory, please add the filenames to
the above list.